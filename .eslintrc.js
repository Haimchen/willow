module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  "extends": "standard",
  "plugins": [
    "html",
    "standard",
    "promise"
  ],
  rules: {
    semi: 0,
    "comma-dangle": ["error", "always-multiline"],
    "space-before-function-paren": ["error", "never"],
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
  },
  globals: {
    _: true,
    $: true,
    jQuery: true,
  },
};
