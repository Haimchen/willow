# Willow - Medical Decision Trees Visualization

### Usage


**Production**

Static files can be found in dist/static.

With Ruby installed  a simple webserver can be startet with the command

    ./server.sh

This serves the app on Port 9999.

Note: The application requires access to the backend server to retrieve data.
The server can be reached from inside the TU Berlin network.


**Dev**

Requires npm and all Node Modules specified in package.json.
To build and run the application in development mode run:

    npm install
    npm run dev

In this case the application can also be used with static files:

    OFFLINE=1 npm run dev

All data is retrieved from static files which means that editing is not possible.
Please be aware that not all data is available in offline mode.



### API

Server:
    10.200.1.75:8012 == Tree Database
    10.200.1.75:8016 == Patients, User Database

#### GET tree

    /tree?hops=3&name=diarrhea&rootNodeId=2


#### Tree list

    /graphs/all-bns

#### Patient list

    /patients/all-ids

    /patients/all


#### GET patient

    /patients/id/{id}
    http://10.200.1.75:8016/patients/id/1479895029377

#### Login

    /users/login
    10.200.1.75:8016/users/login?username="uwe.wagner@test.com"&password="admin"


#### Editing Trees

Delete Node
@RequestMapping("/graph/vertex/remove/{id}")
   public void removeVertex(@PathVariable("id") String id, @RequestParam(value = "name", defaultValue = "graphdiarrhea1") String graphName) {

Delete Edge
@RequestMapping("/graph/edge/remove")
   public void removeEdge(@RequestParam("src") String src, @RequestParam("dst") String dst, @RequestParam(value = "name", defaultValue
           = "graphdiarrhea1") String graphName) {

Create Node
@RequestMapping("/graph/vertex/add")
   public Vertex addVertex(@RequestParam(value = "name", defaultValue = "graphdiarrhea1") String graphName) {

Create Edge
@RequestMapping("/graph/edge/add")
   public Edge addEdge(@RequestParam("src") String src, @RequestParam("dst") String dst, @RequestParam(value = "name", defaultValue
           = "graphdiarrhea1") String graphName) {

Edit Node
@RequestMapping("/tree/vertex/edit/{id}")
   public ResponseEntity updateTreeVertex(@PathVariable("id") String id,
           @RequestParam(value = "name", defaultValue = "graphdiarrhea1") String graphName,
           @RequestParam(value = "properties", required = true) String properties)

Properties are passed as a json map


### Create tree

Copy Tree
@RequestMapping("/graph/copy")
   public void copyGraph(@RequestParam(value = "name", defaultValue = "graphdiarrhea1") String graphName, @RequestParam(name = "target",
           required = true) String renamedGraph, @RequestParam(value = "type", defaultValue = "tree") String type)

Create tree
@RequestMapping("/graph/add")
   public void copyGraph(@RequestParam(value = "name", required = true) String graphName, @RequestParam(value = "type", defaultValue
           = "tree") String type)
