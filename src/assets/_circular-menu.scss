@import "_circular-menu-helper";

// -----------------------------------------------------------------------------
//
// Configuration
//
// Configure the variables below to override defaults. If you want to tweak any
// others, scroll through the individual sections below. More info about these
// variables can be found by reading through the section comments, bue here's
// a general rundown:
//
// 1. Set up the initial menu item radius.
// 2. Decalare how many items our menu will contain.
// 3. Set up a theme colour.
// 4. The spread radius, which is how far the menu items spread from the origin.
// 5. The delay increment, which is how much delay there is between each menu
//    item leaving from / returning to the origin.
// 6. The position of the menu, chosen from one of four values:
//        `bottom-right`   - bottom right corner (this is the default)
//        `bottom-left`    - bottom left corner
//        `top-left`       - top left corner
//        `top-right`      - top right corner
// 7. Minimum height at which menu increases size.
// 8. Minimum width at which menu increases size.
// 9. The height of a bar in the toggle button.
// 10. The spacing between bars in the toggle button.
// 11. The padding between the left and right of the toggle button container and
//     the bars.
//
// -----------------------------------------------------------------------------

// $menu-item-radius: 48px;                  [1]
// $num-items: 5;                            [2]
// $menu-theme-color: rgb(255, 40, 60);      [3]
// $spread-radius: 144px;                    [4]
// $delay-increment: 0.1s;                   [5]
// $menu-position: "bottom-right";           [6]
// $mq-height: 480px;                        [7]
// $mq-width: 480px;                         [8]
// $button-bar-height: 4px;                  [9]
// $button-bar-spacing: 4px;                 [10]
// $button-lr-padding: 10px;                 [11]

// -----------------------------------------------------------------------------
//
// Menu Items Setup
//
// Some configuration for the appearance of the menu items. These include:
//
// 1. Set up the initial menu item radius.
// 2. Calculate initial diameter as a function of the radius.
// 3. Decalare how many items our menu will contain.
// 4. Set up a theme colour - I chose pink.
//
// -----------------------------------------------------------------------------

$menu-item-radius: 24px !default;                 // [1]
$menu-item-diameter: $menu-item-radius*2;         // [2]
$num-items: 5 !default;                           // [3]
$menu-theme-color: rgb(255, 40, 60) !default;     // [4]

// -----------------------------------------------------------------------------
//
// Menu Positional & Timing Setup
//
// These variables are strictly related to position and timing of menu
// items. It's important to remember that they fly out along a spread radius,
// so the number of items combined with the delays and spread radius will
// be responsible for actually positioning the menu. Here's what is set
// up:
//
// 1. The angular increment between menu items which is a function of the number
//    of menu items.
// 2. The angle, which is initialised to the increment.
// 3. The spread radius, which is how far the menu items spread from the origin.
// 4. The delay increment, which is how much delay there is between each menu
//    item leaving from / returning to the origin.
// 5. The initial delay, which is initialised to the delay increment.
// 6. The "n minus 1 initial delay", which is the initial delay for the n-1
//    menu item.
// 7. The final delay, which is the delay of the final menu item.
//
// -----------------------------------------------------------------------------

// $increment: 90deg/($num-items - 1);                               // [1]
// $angle: $increment;                                               // [2]
$increment: 180deg/($num-items - 1);                               // [1]
$angle: $increment;
$spread-radius: 144px !default;                                   // [3]
$delay-increment: 0.1s !default;                                  // [4]
$initial-delay: $delay-increment;                                 // [5]
$n-minus-1-initial-delay: ($num-items - 2) * $delay-increment;    // [6]
$final-delay: ($num-items - 1) * $delay-increment;                // [7]

// -----------------------------------------------------------------------------
//
// Position Mixin
//
// 2. translate-menu-item, which calculates the coordinates to translate menu
//    items to, based on spread radius and angle.
//
// -----------------------------------------------------------------------------

// [2]

@mixin translate-menu-item($r, $theta, $n) {

  @if ($n == "first") {
    transform: translate(-$r, 0);
  } @else if ($n == "last") {
    transform: translate($r, 0);
  } @else {
    transform: translate(floor($r * cos($theta)), floor(-$r * sin($theta)));
  }
}

/* -----------------------------------------------------------------------------

  Circle Menu Component

----------------------------------------------------------------------------- */

/**
 * This is the actual menu component. It consists of a menu element with an
 * unordered list inside, and also a button to toggle the actual menu.
 * It's fixed to the bottom-right of the screen, and each of the items are
 * positioned absolutely inside the parent menu tag. The default set up above is
 * 5 menu items. Because all the transforms and such are calculated wrt
 * this number, you'll need to edit it accordingly depending on how many items
 * you decide to put in the markup.
 *
 * Example markup:
 *
 * <menu class="c-circle-menu">
 *   <ul class="c-circle-menu__items">
 *     <li class="c-circle-menu__item">
 *       <a class="c-circle-menu__link"></a>
 *     </li>
 *     <li class="c-circle-menu__item">
 *       <a class="c-circle-menu__link"></a>
 *     </li>
 *     ...
 *   </ul>
 * </menu>
 */

.c-circle-menu {
  position: absolute;
  // @include get-menu-position;
  z-index: 1000;
  width: $menu-item-diameter;
  height: $menu-item-diameter;
  border-radius: $menu-item-radius;
}

.c-circle-menu__items {
  display: block;
  list-style: none;
  position: absolute;
  z-index: 2;
  margin: 0;
  padding: 0;
}

.c-circle-menu__item {
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  width: $menu-item-diameter;
  height: $menu-item-diameter;
  border-radius: $menu-item-radius;
  opacity: 0;
  transition: transform, opacity;
  transition-duration: 0.3s, 0.3s;
  transition-timing-function: cubic-bezier(.35, -.59, .47, .97);
}

/**
 * Transisition delays at the default state.
 */

.c-circle-menu__item:nth-child(1) {
  transition-delay: $final-delay;
}

@for $i from 2 through ($num-items) {
  .c-circle-menu__item:nth-child(#{$i}) {
    transition-delay: $n-minus-1-initial-delay;
  }
  $n-minus-1-initial-delay: $n-minus-1-initial-delay - $delay-increment;
}

.c-circle-menu__item:nth-child(#{$num-items}) {
  transition-delay: 0s;
}

/**
 * We're using the .is-active class, which is added to the menu via JavaScript.
 * Once the menu is active, the items inherit the properties below. We will
 * manually write out the transform properties for first and last items, as we
 * already know their position. For all items in between though, we'll use some
 * polar-to-cartesian math and some Sass functions to get the positioning.
 */

.c-circle-menu .c-circle-menu__item {
  transition-timing-function: cubic-bezier(.35, .03, .47, 1.59);
}

.c-circle-menu .c-circle-menu__item:nth-child(1) {
  transition-delay: 0s;
  @include translate-menu-item($spread-radius, 0, "first");
}

@for $i from 2 through ($num-items - 1) {
  .circle-menu-enter .c-circle-menu .c-circle-menu__item:nth-child(#{$i}) {
    transition-delay: $initial-delay;
  }

  .c-circle-menu .c-circle-menu__item:nth-child(#{$i}) {
    @include translate-menu-item($spread-radius, $angle, "");
  }
  $initial-delay: $initial-delay + $delay-increment;
  $angle: $angle + $increment;
}

.c-circle-menu .c-circle-menu__item:nth-child(#{$num-items}) {
  transition-delay: $final-delay;
  @include translate-menu-item($spread-radius, 90deg, "last");
//
//   @include mq {
//     @include translate-menu-item($spread-radius * 2, 90deg, "last");
//   }
}

/**
 * Apart from the transform properties, we'll also make sure the items get
 * the correct opacity.
 */

.c-circle-menu .c-circle-menu__item {
  opacity: 1;
}

/**
 * Let's style the links now. This is just boilerplate stuff, and of course,
 * you'll probably want to change up the icons to match your needs. In any case,
 * we'll do it here for the sake of completion.
 */

.c-circle-menu__link {
  display: block;
  width: 100%;
  height: 100%;
  border-radius: $menu-item-radius;
  box-shadow: inset 0 0 0 2px #fff;
}

.c-circle-menu__link img {
  display: block;
  max-width: 100%;
  height: auto;
}

.c-circle-menu__link:hover {
  box-shadow: inset 0 0 0 2px $menu-theme-color;
}
