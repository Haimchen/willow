var urls = {
  getTreeUrl() { return 'http://10.200.1.75:8012/tree' },
  getTreeListUrl() { return 'http://10.200.1.75:8016/graphs/all-trees' },

  getNodeDeleteUrl(id) { return 'http://10.200.1.75:8012/graph/vertex/remove/' + id },
  getNodeUpdateUrl(id) { return 'http://10.200.1.75:8012/tree/vertex/edit/' + id },
  getNewNodeUrl() { return 'http://10.200.1.75:8012/graph/vertex/add' },
  getNewEdgeUrl() { return 'http://10.200.1.75:8012/graph/edge/add' },
  getDeleteEdgeUrl() { return 'http://10.200.1.75:8012/graph/edge/remove' },

  getNewTreeUrl() { return 'http://10.200.1.75:8012/graph/add' },
  getCopyTreeUrl() { return 'http://10.200.1.75:8012/graph/copy' },

  getPatientUrl(id) { return 'http://10.200.1.75:8016/patients/id/' + id },
  getPatientListUrl() { return 'http://10.200.1.75:8016/patients/all' },

  postLoginUrl() { return 'http://10.200.1.75:8016/users/login' },
}

if (process && process.env.OFFLINE) {
  console.warn('Using static fixtures for webservice calls!');
  urls = {
    getTreeUrl(id) { return '/static/tree/' + id + '.json' },
    getTreeListUrl() { return '/static/graphs/all-trees.json' },

    getPatientUrl(id) { return '/static/patients/id/' + id + '.json' },
    getPatientListUrl() { return '/static/patients/all.json' },

    postLoginUrl() { return '/static/users/login' },
  }
}

export default urls;
