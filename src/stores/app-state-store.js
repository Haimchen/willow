var superagent = require('superagent');

import webserviceUrls from 'src/webservice_urls'
import PatientStore from 'stores/patient-store'
import TreeStore from 'stores/tree-store'

export default {
  state: {
    user: null,
    loggingIn: false,
    messages: [],
  },
  // maybe connect tree and patient stores?
  setUser(user) {
    this.state.user = user;
  },
  setMessage(text, error) {
    var message = {text: text, error: error};
    this.state.messages.push(message);
    setTimeout(() => {
      this.state.messages.shift();
    }, 10000);
  },
  logout() {
    // maybe clear Tree and Patient stores?
    PatientStore.clearPatient();
    TreeStore.clearTree();
    this.state.user = null;
  },
  login(username, password) {
    this.state.loggingIn = true;
    superagent.post(webserviceUrls.postLoginUrl())
      .query({username: username, password: password})
      .timeout(10000)
      .end((err, res) => {
        this.state.loggingIn = false;
        if (err) { return this.handleLoginError(err, res); }
        var user = res.body;
        user.name = user.firstname + ' ' + user.lastname;
        this.state.user = user;
      });
  },
  resetSelection() {
    PatientStore.clearPatient();
    TreeStore.clearTree();
  },
  handleLoginError(err, res) {
    var message = err.toString();
    if (res && res.statusCode === 403) {
      message = 'Invalid Login Data, the username or password you entered is not correct.';
    }
    this.setMessage(message, true);
  },
}
