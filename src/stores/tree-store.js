var superagent = require('superagent');

import webserviceUrls from 'src/webservice_urls'
import appState from 'stores/app-state-store'
import PatientStore from 'stores/patient-store'

export default {
  state: {
    // original tree object
    tree: null,
    treeId: null,
    infoNode: null,
    selectedNode: null,
    menuNode: null,
    trees: null,
    moving: false,
  },
  // Contains all info necessary to perform a move of a node (node to move, oldParent, newParent)
  _moveInfo: null,
  _selectedNodeId: null,
  // D3 tree object
  _tree: null,
  // tree is a Tree provided by D3
  setD3Tree(tree) {
    // removes all svg elements of the old tree (if we don't do that, we get two trees at once)
    if (this._tree) {
      this._tree.clear();
    };
    this._tree = tree;
    if (tree && this._nodeInfos) this._tree.setNodeInfos(this._nodeInfos);
    if (tree && this._selectedNodeId) {
      var node = this._tree.getNodeById(this._selectedNodeId);
      this.selectNode(node);
    }
  },
  setNodeInfos(newInfos) {
    this._nodeInfos = newInfos;
    this._tree && this._tree.setNodeInfos(newInfos);
  },
  // tree is a Tree Object as provided by the API
  setJsonTree(tree, treeId) {
    if (tree && treeId) {
      this.state.tree = tree;
      this.state.treeId = treeId;
    } else {
      appState.setMessage('You can only set a tree if you also set the id!', true);
    }
  },
  loadTree(id, cache = true) {
    // if we already have the right tree > do nothing
    if (cache) {
      if (this.state.tree && this.state.treeId && id === this.state.treeId) { return; }
    }
    // If we need to load a new tree > clear everything before
    this.clearTree();
    var prom = superagent.get(webserviceUrls.getTreeUrl(id))
      // hop number is 5 because backend can't handle more for hcc graph
      // another possible paramter: rootNodeId
      .query({hops: 5, name: id})
      .timeout(10000);

    prom.then((res) => {
      if (typeof res.text === 'string') {
        res.text = JSON.parse(res.text);
      }
      this.setJsonTree(res.text[0], id);
    });
    prom.catch((err) => {
      if (err) { return this.handleTreeError(err, id); }
    });
    return prom;
  },
  amendTree(parentNode) {
    // take selected Tree and given Node and try to load more children from selected node
    // check if selected node already has children?
    if (parentNode.children || parentNode._children) {
      return;
    }
    var id = this.state.treeId;
    var rootId = parentNode.data.properties.id;
    if (!id || !rootId) { return; }
    // API call with selected node as root
    var prom = superagent.get(webserviceUrls.getTreeUrl(id))
      .query({hops: 2, name: id, rootNodeId: rootId})
      .timeout(10000);

    prom.then((res) => {
      if (typeof res.text === 'string') {
        res.text = JSON.parse(res.text);
      }
      // response is array, first element is requested root Node with new children
      var newChildren = res.text[0].children;
      if (!newChildren || newChildren.length === 0) { return; };
      // start recursion at original root
      var rootNode = this.state.tree;
      // use recursion to walk through tree and append new data (see below) to all nodes with rootId
      this._appendChildren(rootNode, newChildren, rootId);
      // D3 tree should get updates automatically by watch in tree manager

      var newTree = this.state.tree;
      this.state.tree = {};
      this.state.tree = newTree;
    });
    prom.catch((err) => {
      if (err) { return this.handleTreeError(err, id); }
    });
  },
  _appendChildren(parentNode, newChildren, rootId) {
    if (!parentNode.children || parentNode.children.length === 0) {
      return;
    }
    parentNode.children.forEach((node) => {
      if (node.properties.id === rootId && (!node.children || node.children.length === 0)) {
        node.children = newChildren;
      }
      this._appendChildren(node, newChildren, rootId);
    });
  },
  reloadTree() {
    return this.loadTree(this.state.treeId, false);
  },
  clearTree() {
    this.state.treeId = null;
    this.state.tree = null;
    if (this._tree) { this._tree.clear(); }
    this._tree = null;
  },
  selectNode(node) {
    // If we started moving a node, the normal flow is not performed
    // we chose the selected node as new parent instead
    if (this.state.moving) {
      return this.selectMoveTarget(node);
    }
    if (!node) { return; }
    this._selectedNodeId = node.data.properties.id;
    if (node === this.state.menuNode) {
      this.state.menuNode = null;
      this.state.selectedNode = null;
    } else if (node === this.state.selectedNode) {
      this.state.menuNode = node;
    } else {
      this.state.menuNode = null;
      this.state.selectedNode = node;
    }
    this.state.infoNode = node;
    PatientStore.updatePatientNodeInfo(node.data.name);

    // this method also updates the tree
    this._tree.setSelectedNode(node);
  },
  toggleChildNodes(node) {
    this._tree.toggleChildNodes(node);
  },
  startMoving(params) {
    this.state.moving = true;
    this._moveInfo = params;
  },
  selectMoveTarget(node) {
    if (!node) { return; }
    var newParent = node.data.properties.id;
    // check if new parent is not equal to oldParent
    if (!newParent || newParent === this._moveInfo.oldParent) { return; }
    this._moveInfo.newParent = newParent;
    this.moveNode();
  },
  cancelMove() {
    this.state.moving = false;
  },
  moveNode() {
    var moveNodeId = this._moveInfo.moveNode;
    var oldParentId = this._moveInfo.oldParent;
    var newParentId = this._moveInfo.newParent;
    if (!moveNodeId || !oldParentId || !newParentId) { return; }

    Promise.all([
      // delete old edge
      superagent
        .get(webserviceUrls.getDeleteEdgeUrl())
        .query({name: this.state.treeId, src: oldParentId, dst: moveNodeId})
        .timeout(10000),

      // create new edge
      superagent
        .get(webserviceUrls.getNewEdgeUrl())
        .query({name: this.state.treeId, src: newParentId, dst: moveNodeId})
        .timeout(10000),
    ])
      .then(() => {
        this.state.menuNode = null;
        return this.reloadTree();
      })
      .then(() => {
        this.state.moving = false;
      })
      .catch((err) => {
        this.handleEditError(err);
        this.state.moving = false;
      });
  },
  deleteNode(node) {
    if (!node) { return; }

    var nodeId = node.data.properties.id;
    superagent.get(webserviceUrls.getNodeDeleteUrl(nodeId))
      .query({name: this.state.treeId})
      .timeout(10000)
      .end((err, res) => {
        if (err) { return this.handleEditError(err); }
        this.reloadTree();
      });
  },
  updateNode(nodeId, newProps) {
    if (!nodeId || !newProps) { return; }

    var props = JSON.stringify(newProps);
    return superagent.get(webserviceUrls.getNodeUpdateUrl(nodeId))
      .query({name: this.state.treeId, properties: props})
      .timeout(10000)
      .then((res) => {
        // Res ist leer
        return this.reloadTree();
      })
      .then(() => {
        this._selectedNodeId = nodeId;
      })
      .catch((err) => {
        if (err) { return this.handleEditError(err); }
      });
  },
  newNode(parentNode) {
    if (!parentNode) { return; }
    var parentId = parentNode.data.properties.id;
    // create new empty Node
    superagent.get(webserviceUrls.getNewNodeUrl())
      .query({name: this.state.treeId})
      .timeout(10000)
      .end((err, res) => {
        if (err) { return this.handleEditError(err); }
        // get new node's Id
        var newNode = res.body.id;
        // create edge from parent to new Node
        superagent.get(webserviceUrls.getNewEdgeUrl())
          .query({name: this.state.treeId, src: parentId, dst: newNode})
          .end((err, res) => {
            if (err) { return this.handleEditError(err); }
            // reload Graph
            this.reloadTree()
              .then(() => {
                this._selectedNodeId = newNode;
              });
          });
      });
  },
  copyTree(id, newName) {
    if (!id || !newName) { return; }
    return superagent.get(webserviceUrls.getCopyTreeUrl())
      .query({name: id, target: newName, type: 'tree'})
      .timeout(10000)
      .then((res) => {
        this.fetchTreesList();
        // hier die neue ID zurückgeben
        return res.text;
      })
     .catch((err) => { this.handleTreesError(err); });
  },
  newTree(params) {
    if (!params || !params.name) { return; }
    return superagent.get(webserviceUrls.getNewTreeUrl())
      .query({name: params.name, type: 'tree'})
      .timeout(10000)
      .then((res) => {
        this.fetchTreesList();
        return res.text;
      })
     .catch((err) => { this.handleTreesError(err); });
  },
  loadTrees() {
    if (!this.state.trees) { this.fetchTreesList(); }
  },
  fetchTreesList() {
    return superagent.get(webserviceUrls.getTreeListUrl())
      .timeout(10000)
      .then((res) => {
        var resTrees = res.body;

        resTrees = resTrees.map((tree) => {
          tree.name = tree.graphName;
          tree.description = 'Not edited';
          return tree;
        });
        this.state.trees = resTrees;
      })
    .catch((err) => {
      return this.handleTreesError(err);
    });
  },
  handleTreeError(err, id) {
    var message = 'Could not load Tree ' + id + '. ' + err.toString();
    appState.setMessage(message, true);
  },
  handleTreesError(err, id) {
    var message = 'Could not load Tree List: ' + err.toString();
    appState.setMessage(message, true);
  },
  handleEditError(err) {
    var message = 'Could not edit tree: ' + err.toString();
    appState.setMessage(message, true);
  },
  filterTrees(filterList) {
    // TODO: filter function may need to change (if name != the displayed name)
    var filteredTrees = this.state.trees.filter(function(tree) {
      return filterList.includes(tree.graphDBId);
    });
    return filteredTrees;
  },

}
