var superagent = require('superagent');

import webserviceUrls from 'src/webservice_urls'
import appState from 'stores/app-state-store'

export default {
  state: {
    // patient with loaded data
    chosenPatient: null,
    // List with graph Names for chosen Patient (only names, no data!)
    patientTrees: [],
    // original List with all patients from api call
    patientsJson: null,
    // stores information about the current info node for the chosen patient
    patientNodeInfo: null,
  },
  loadPatients() {
    if (!this.state.patientsJson) { this.fetchPatientsList(); }
  },
  loadPatient(id) {
    if (this.state.chosenPatient && id === this.state.chosenPatient.id) { return; }
    // if we need to fetch a new patient > clear store first so that no patient is set
    this.clearPatient();
    superagent.get(webserviceUrls.getPatientUrl(id))
      .timeout(10000)
      .end((err, res) => {
        if (err) { return this.handlePatientError(err, res); }
        var patient = res.body;
        patient = this.preprocessPatient(patient);

        this.state.chosenPatient = patient;
        this.updatePatientTrees();
      });
  },
  // Takes the patient from the API
  // filters medical Actions and removes duplicates
  preprocessPatient(patient) {
    var sortedActions = _.sortBy(patient.medicalActions, (action) => { return action.timestamp });
    var uniqueActions = _.uniqBy(sortedActions, (action) => {
      // TODO: Nochmal anregen, dass die Action auch die NodeID kennt....
      return action.action.name;
    });
    patient.medicalActions = uniqueActions;

    return patient;
  },
  clearPatient() {
    this.patientTrees = [];
    this.state.patientNodeInfo = null;
    this.state.chosenPatient = null;
  },
  updatePatientTrees() {
    if (!this.state.chosenPatient) { return; }
    var patientTreeNames = [];
    this.state.chosenPatient.medicalActions.forEach(function(entry) {
      patientTreeNames.push(entry.action.graphId);
    })
    this.state.patientTrees = patientTreeNames;
  },
  updatePatientNodeInfo(nodeName) {
    if (!this.state.chosenPatient) {
      this.state.patientNodeInfo = null;
      return;
    }
    var entry = this.state.chosenPatient.medicalActions.find((entry) => { return entry.action.name === nodeName; });
    if (!entry) {
      this.state.patientNodeInfo = null;
      return;
    }

    var nodeInfo = {};
    nodeInfo['timestamp'] = entry.timestamp;
    nodeInfo['result'] = entry.state;
    nodeInfo['physician'] = {
      id: entry.physician.id,
      lastname: entry.physician.lastname,
      firstname: entry.physician.firstname,
    }
    this.state.patientNodeInfo = nodeInfo;
  },
  fetchPatientsList() {
    superagent.get(webserviceUrls.getPatientListUrl())
      .timeout(10000)
      .end((err, res) => {
        if (err) { return this.handlePatientsError(err, res); }
        this.state.patientsJson = res.body;
      });
  },
  handlePatientsError(err, res) {
    var message = 'Could not get Patient List. ' + err.toString();
    appState.setMessage(message, true);
  },
  handlePatientError(err, res) {
    var message = 'Could not get Patient data. ' + err.toString();
    appState.setMessage(message, true);
  },
  handleError(err, res) {
    var message = 'Could not get Patient Data.' + err.toString();
    appState.setMessage(message, true);
  },
}
