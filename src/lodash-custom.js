import _ from 'lodash/core';

var lodashCustom = _.extend({}, _);
lodashCustom.debounce = require('lodash/debounce');
lodashCustom.uniqBy = require('lodash/uniqBy');

module.exports = lodashCustom;
