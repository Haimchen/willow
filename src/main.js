// var d3 = require('d3')
require('font-awesome/css/font-awesome.css')
require('bootstrap-loader/lib/bootstrap.loader?configFilePath=../../../.bootstraprc!bootstrap-loader/no-op.js')

import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

import App from 'components/app'
import AppState from 'stores/app-state-store'
// import TreeStore from 'stores/tree-store'

import Selection from 'components/selection'
import Login from 'components/login'
import TreeManager from 'components/tree-manager'
import TreeCreator from 'components/tree-creator'
import PatientCreator from 'components/patient-creator'
import ErrorPage from 'components/error-page'

// Fake User, for easier testing of tree vis components
AppState.setUser({name: 'Heinz Grummel'});

// Create Router with Route Definitions
const router = new Router({
  routes: [
    { path: '/', component: Selection },
    { path: '/login', component: Login },
    { path: '/tree/new', name: 'newTree', component: TreeCreator },
    { path: '/tree/:treeId', name: 'tree', component: TreeManager },
    { path: '/patient/new', name: 'newPatient', component: PatientCreator },
    { path: '/tree/:treeId/patient/:patientId', name: 'patient', component: TreeManager },
    { path: '/*', component: ErrorPage },
  ],
  base: '/',
});

// Create App (root instance)
/* eslint-disable no-unused-vars */
var app = new Vue({
  el: '#app',
  router: router,
  render: h => { return h(App); },
});
