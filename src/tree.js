var d3 = _.extend({},
  require('d3-transition'),
  require('d3-selection'),
  require('d3-hierarchy'),
  require('d3-shape'),
)
import TreeStore from 'stores/tree-store'

const animationDuration = 500;
var globalId = 1;
function setUniqueId(d) {
  // Database gives us no unique ID, so we need to assign one
  // database-ids can appear multiple times per tree (circles)
  // ids are newly generated on each request
  return d._id || (d._id = globalId++);
}

function getShortName(d) {
  var name = d.data.name;
  if (name.length > 16) {
    var spacePos = name.indexOf(' ', 7);
    if (spacePos < 0 || spacePos > 13) {
      name = name.slice(0, 11) + '...';
    } else {
      name = name.slice(0, spacePos);
    }
  }
  return name;
}

function textPositionLeft(d) {
  var left = true;
  if (!d.parent) {
    left = false;
  }
  return left;
}

module.exports = (domId, data) => {
  var svg = d3.select(domId);
  var width = +svg.attr('width');
  var height = +svg.attr('height');
  var g = svg.append('g').attr('transform', 'translate(25,0)');
  var selectedNode = null;
  var tree = d3.tree()
      .size([height, width - 55]);

  var originalData = null;

  originalData = d3.hierarchy(data);

  var nodeClass = function(d) {
    var classes = ['node'];
    // classes for internal nodes and leaves
    if (!d.parent) {
      // this is the root
      // for the moment it gets the same class as leaves
      classes.push('node--left');
    } else if (d.children) {
      classes.push('node--internal');
    } else {
      classes.push('node--left');
    }
    // class for hidden children
    if (d._children ||
        ((!d.children || d.children.size === 0) && d.data.properties['out-degree'] > 0)) {
      classes.push('hidden-children');
    }
    // classes per type of node
    var type = d.data.type;
    classes.push(type);

    // classes for active nodes
    if (selectedNode) {
      // selected node itself
      if (d._id === selectedNode._id) { classes.push('selected'); }
      // parent of selected node
      if (d.children) {
        var childrenIds = d.children.map((child) => { return child._id; });
        if (childrenIds.includes(selectedNode._id)) { classes.push('selected-neighbor'); }
      }
      // children of selected node
      if (d.parent && d.parent._id === selectedNode._id) { classes.push('selected-neighbor'); }

      // nodes with same db-id as selected node
      var id = selectedNode.data.properties['titan-db-id'];
      if (d.data.properties['titan-db-id'] === id) {
        classes.push('selected-twin');
      }
    }
    // classes for patient data
    var nodeInfo = d3Tree._nodeInfos[d.data.name];
    if (nodeInfo) {
      nodeInfo.medicalActionPositive ? classes.push('action-pos') : classes.push('action-neg');
    } else {
      classes.push('no-action');
    }

    return classes.join(' ');
  };

  var d3Tree = {
    _nodeInfos: {},
    update(source) {
      var root = originalData;
      root.x0 = 250;
      root.y0 = 0;
      root.each((n) => {
        setUniqueId(n);
        n._shortName = getShortName(n);
      });

      var link = g.selectAll('.link')
        .data(tree(root).descendants().slice(1), function(d) {
          return d._id;
        });

      link.transition()
        .duration(animationDuration)
        .attr('d', function(d) {
          return 'M' + d.y + ',' + d.x +
            'C' + (d.y + d.parent.y) / 2 + ',' + d.x +
            ' ' + (d.y + d.parent.y) / 2 + ',' + d.parent.x +
            ' ' + d.parent.y + ',' + d.parent.x;
        });
      /* eslint-disable no-unused-vars */
      var linkExit = link.exit()
          .remove();

      /* eslint-disable no-unused-vars */
      var linkEnter = link
        .enter().append('path')
          .attr('class', 'link')
          .attr('d', function(d) {
            return 'M' + (source || d).y0 + ',' + (source || d).x0 +
              'C' + ((source || d).y0 + (source || d.parent).y0) / 2 + ',' + (source || d).x0 +
              ' ' + ((source || d).y0 + (source || d.parent).y0) / 2 + ',' + (source || d.parent).x0 +
              ' ' + (source || d.parent).y0 + ',' + (source || d.parent).x0;
          })
        .transition()
          .duration(animationDuration)
          .attr('d', function(d) {
            return 'M' + d.y + ',' + d.x +
              'C' + (d.y + d.parent.y) / 2 + ',' + d.x +
              ' ' + (d.y + d.parent.y) / 2 + ',' + d.parent.x +
              ' ' + d.parent.y + ',' + d.parent.x;
          });

      var node = g.selectAll('.node')
        .data(tree(root).descendants(), function(d) { return d._id; })
        .attr('class', nodeClass);

      var nodeUpdate = node.transition()
          .duration(animationDuration)
          .attr('transform', function(d) { return 'translate(' + d.y + ',' + d.x + ')'; });

      var nodeEnter = node
        .enter().append('g')
          .attr('transform', function(d) { return 'translate(' + (source || d).y0 + ',' + (source || d).x0 + ')'; })
          .attr('class', nodeClass)
          .on('click', function(d, i) {
            TreeStore.selectNode(d);
          });

      nodeEnter.transition()
        .duration(animationDuration)
        .attr('transform', function(d) { return 'translate(' + d.y + ',' + d.x + ')'; });

      nodeEnter.append('circle')
          .attr('r', 5);

      // Indicators for hidden nodes
      nodeEnter
        .append('path')
        .attr('class', 'triangle')
        .attr('d', d3.symbol().size(20).type(d3.symbolTriangle))
        .attr('transform', 'translate(11,0) rotate(210)');

      nodeEnter.append('text')
          .attr('dy', -10)
          .attr('x', function(d) { return textPositionLeft(d) ? -10 : 10; })
          .style('text-anchor', function(d) { return textPositionLeft(d) ? 'end' : 'start'; })
          .attr('class', 'node-text')
          .text(function(d) { return d._shortName; })
          .on('mouseover', function(d, i) {
            d3.select(this)
              .text(function(d) { return d.data.name; });
          })
          .on('mouseout', function(d) {
            d3.select(this)
              .text(d => d._shortName);
          });

      /* eslint-disable no-unused-vars */
      var nodeExit = node.exit().transition()
          .duration(animationDuration)
          .attr('transform', function(d) { return 'translate(' + (source || d.parent).y + ',' + (source || d.parent).x + ')'; })
          .remove();
      root.each(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;
      });
    },

    toggleChildNodes(node) {
      if (node.children && node.children.length > 0) {
        node._children = node.children;
        node.children = null;
      } else if (node._children) {
        node.children = node._children;
        node._children = null;
      } else if (node.data.properties['out-degree'] > 0) {
        TreeStore.amendTree(node);
      }
      this.update(node);
    },
    setNodeInfos(map) {
      this._nodeInfos = map;
      this.update();
    },
    setSelectedNode(node) {
      selectedNode = node;
      this.update();
    },
    getNodeById(id) {
      var result;
      originalData.each((node) => {
        if (node.data.properties.id === id) { result = node; }
      });
      return result;
    },
    clear() {
      g.selectAll('*').remove();
    },
  }
  d3Tree.update(originalData);
  return d3Tree;
}
