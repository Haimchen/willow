webpackJsonp([2,0],[
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _vue = __webpack_require__(286);

	var _vue2 = _interopRequireDefault(_vue);

	var _vueRouter = __webpack_require__(285);

	var _vueRouter2 = _interopRequireDefault(_vueRouter);

	var _app = __webpack_require__(260);

	var _app2 = _interopRequireDefault(_app);

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _selection = __webpack_require__(82);

	var _selection2 = _interopRequireDefault(_selection);

	var _login = __webpack_require__(80);

	var _login2 = _interopRequireDefault(_login);

	var _treeManager = __webpack_require__(83);

	var _treeManager2 = _interopRequireDefault(_treeManager);

	var _treeCreator = __webpack_require__(269);

	var _treeCreator2 = _interopRequireDefault(_treeCreator);

	var _patientCreator = __webpack_require__(268);

	var _patientCreator2 = _interopRequireDefault(_patientCreator);

	var _errorPage = __webpack_require__(264);

	var _errorPage2 = _interopRequireDefault(_errorPage);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	__webpack_require__(148);
	__webpack_require__(102);

	_vue2.default.use(_vueRouter2.default);

	_appStateStore2.default.setUser({ name: 'Heinz Grummel' });

	var router = new _vueRouter2.default({
	  routes: [{ path: '/', component: _selection2.default }, { path: '/login', component: _login2.default }, { path: '/tree/new', name: 'newTree', component: _treeCreator2.default }, { path: '/tree/:treeId', name: 'tree', component: _treeManager2.default }, { path: '/patient/new', name: 'newPatient', component: _patientCreator2.default }, { path: '/tree/:treeId/patient/:patientId', name: 'patient', component: _treeManager2.default }, { path: '/*', component: _errorPage2.default }],
	  base: '/'
	});

	var app = new _vue2.default({
	  el: '#app',
	  router: router,
	  render: function render(h) {
	    return h(_app2.default);
	  }
	});

/***/ },
/* 1 */,
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _webservice_urls = __webpack_require__(31);

	var _webservice_urls2 = _interopRequireDefault(_webservice_urls);

	var _patientStore = __webpack_require__(7);

	var _patientStore2 = _interopRequireDefault(_patientStore);

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var superagent = __webpack_require__(44);

	exports.default = {
	  state: {
	    user: null,
	    loggingIn: false,
	    messages: []
	  },
	  setUser: function setUser(user) {
	    this.state.user = user;
	  },
	  setMessage: function setMessage(text, error) {
	    var _this = this;

	    var message = { text: text, error: error };
	    this.state.messages.push(message);
	    setTimeout(function () {
	      _this.state.messages.shift();
	    }, 10000);
	  },
	  logout: function logout() {
	    _patientStore2.default.clearPatient();
	    _treeStore2.default.clearTree();
	    this.state.user = null;
	  },
	  login: function login(username, password) {
	    var _this2 = this;

	    this.state.loggingIn = true;
	    superagent.post(_webservice_urls2.default.postLoginUrl()).query({ username: username, password: password }).timeout(10000).end(function (err, res) {
	      _this2.state.loggingIn = false;
	      if (err) {
	        return _this2.handleLoginError(err, res);
	      }
	      var user = res.body;
	      user.name = user.firstname + ' ' + user.lastname;
	      _this2.state.user = user;
	    });
	  },
	  resetSelection: function resetSelection() {
	    _patientStore2.default.clearPatient();
	    _treeStore2.default.clearTree();
	  },
	  handleLoginError: function handleLoginError(err, res) {
	    var message = err.toString();
	    if (res && res.statusCode === 403) {
	      message = 'Invalid Login Data, the username or password you entered is not correct.';
	    }
	    this.setMessage(message, true);
	  }
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _stringify = __webpack_require__(100);

	var _stringify2 = _interopRequireDefault(_stringify);

	var _promise = __webpack_require__(101);

	var _promise2 = _interopRequireDefault(_promise);

	var _webservice_urls = __webpack_require__(31);

	var _webservice_urls2 = _interopRequireDefault(_webservice_urls);

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _patientStore = __webpack_require__(7);

	var _patientStore2 = _interopRequireDefault(_patientStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var superagent = __webpack_require__(44);

	exports.default = {
	  state: {
	    tree: null,
	    treeId: null,
	    infoNode: null,
	    selectedNode: null,
	    menuNode: null,
	    trees: null,
	    moving: false
	  },

	  _moveInfo: null,
	  _selectedNodeId: null,

	  _tree: null,
	  setD3Tree: function setD3Tree(tree) {
	    if (this._tree) {
	      this._tree.clear();
	    };
	    this._tree = tree;
	    if (tree && this._nodeInfos) this._tree.setNodeInfos(this._nodeInfos);
	    if (tree && this._selectedNodeId) {
	      var node = this._tree.getNodeById(this._selectedNodeId);
	      this.selectNode(node);
	    }
	  },
	  setNodeInfos: function setNodeInfos(newInfos) {
	    this._nodeInfos = newInfos;
	    this._tree && this._tree.setNodeInfos(newInfos);
	  },
	  setJsonTree: function setJsonTree(tree, treeId) {
	    if (tree && treeId) {
	      this.state.tree = tree;
	      this.state.treeId = treeId;
	    } else {
	      _appStateStore2.default.setMessage('You can only set a tree if you also set the id!', true);
	    }
	  },
	  loadTree: function loadTree(id) {
	    var _this = this;

	    var cache = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	    if (cache) {
	      if (this.state.tree && this.state.treeId && id === this.state.treeId) {
	        return;
	      }
	    }

	    this.clearTree();
	    var prom = superagent.get(_webservice_urls2.default.getTreeUrl(id)).query({ hops: 5, name: id }).timeout(10000);

	    prom.then(function (res) {
	      if (typeof res.text === 'string') {
	        res.text = JSON.parse(res.text);
	      }
	      _this.setJsonTree(res.text[0], id);
	    });
	    prom.catch(function (err) {
	      if (err) {
	        return _this.handleTreeError(err, id);
	      }
	    });
	    return prom;
	  },
	  amendTree: function amendTree(parentNode) {
	    var _this2 = this;

	    if (parentNode.children || parentNode._children) {
	      return;
	    }
	    var id = this.state.treeId;
	    var rootId = parentNode.data.properties.id;
	    if (!id || !rootId) {
	      return;
	    }

	    var prom = superagent.get(_webservice_urls2.default.getTreeUrl(id)).query({ hops: 2, name: id, rootNodeId: rootId }).timeout(10000);

	    prom.then(function (res) {
	      if (typeof res.text === 'string') {
	        res.text = JSON.parse(res.text);
	      }

	      var newChildren = res.text[0].children;
	      if (!newChildren || newChildren.length === 0) {
	        return;
	      };

	      var rootNode = _this2.state.tree;

	      _this2._appendChildren(rootNode, newChildren, rootId);


	      var newTree = _this2.state.tree;
	      _this2.state.tree = {};
	      _this2.state.tree = newTree;
	    });
	    prom.catch(function (err) {
	      if (err) {
	        return _this2.handleTreeError(err, id);
	      }
	    });
	  },
	  _appendChildren: function _appendChildren(parentNode, newChildren, rootId) {
	    var _this3 = this;

	    if (!parentNode.children || parentNode.children.length === 0) {
	      return;
	    }
	    parentNode.children.forEach(function (node) {
	      if (node.properties.id === rootId && (!node.children || node.children.length === 0)) {
	        node.children = newChildren;
	      }
	      _this3._appendChildren(node, newChildren, rootId);
	    });
	  },
	  reloadTree: function reloadTree() {
	    return this.loadTree(this.state.treeId, false);
	  },
	  clearTree: function clearTree() {
	    this.state.treeId = null;
	    this.state.tree = null;
	    if (this._tree) {
	      this._tree.clear();
	    }
	    this._tree = null;
	  },
	  selectNode: function selectNode(node) {
	    if (this.state.moving) {
	      return this.selectMoveTarget(node);
	    }
	    if (!node) {
	      return;
	    }
	    this._selectedNodeId = node.data.properties.id;
	    if (node === this.state.menuNode) {
	      this.state.menuNode = null;
	      this.state.selectedNode = null;
	    } else if (node === this.state.selectedNode) {
	      this.state.menuNode = node;
	    } else {
	      this.state.menuNode = null;
	      this.state.selectedNode = node;
	    }
	    this.state.infoNode = node;
	    _patientStore2.default.updatePatientNodeInfo(node.data.name);

	    this._tree.setSelectedNode(node);
	  },
	  toggleChildNodes: function toggleChildNodes(node) {
	    this._tree.toggleChildNodes(node);
	  },
	  startMoving: function startMoving(params) {
	    this.state.moving = true;
	    this._moveInfo = params;
	  },
	  selectMoveTarget: function selectMoveTarget(node) {
	    if (!node) {
	      return;
	    }
	    var newParent = node.data.properties.id;

	    if (!newParent || newParent === this._moveInfo.oldParent) {
	      return;
	    }
	    this._moveInfo.newParent = newParent;
	    this.moveNode();
	  },
	  cancelMove: function cancelMove() {
	    this.state.moving = false;
	  },
	  moveNode: function moveNode() {
	    var _this4 = this;

	    var moveNodeId = this._moveInfo.moveNode;
	    var oldParentId = this._moveInfo.oldParent;
	    var newParentId = this._moveInfo.newParent;
	    if (!moveNodeId || !oldParentId || !newParentId) {
	      return;
	    }

	    _promise2.default.all([superagent.get(_webservice_urls2.default.getDeleteEdgeUrl()).query({ name: this.state.treeId, src: oldParentId, dst: moveNodeId }).timeout(10000), superagent.get(_webservice_urls2.default.getNewEdgeUrl()).query({ name: this.state.treeId, src: newParentId, dst: moveNodeId }).timeout(10000)]).then(function () {
	      _this4.state.menuNode = null;
	      return _this4.reloadTree();
	    }).then(function () {
	      _this4.state.moving = false;
	    }).catch(function (err) {
	      _this4.handleEditError(err);
	      _this4.state.moving = false;
	    });
	  },
	  deleteNode: function deleteNode(node) {
	    var _this5 = this;

	    if (!node) {
	      return;
	    }

	    var nodeId = node.data.properties.id;
	    superagent.get(_webservice_urls2.default.getNodeDeleteUrl(nodeId)).query({ name: this.state.treeId }).timeout(10000).end(function (err, res) {
	      if (err) {
	        return _this5.handleEditError(err);
	      }
	      _this5.reloadTree();
	    });
	  },
	  updateNode: function updateNode(nodeId, newProps) {
	    var _this6 = this;

	    if (!nodeId || !newProps) {
	      return;
	    }

	    var props = (0, _stringify2.default)(newProps);
	    return superagent.get(_webservice_urls2.default.getNodeUpdateUrl(nodeId)).query({ name: this.state.treeId, properties: props }).timeout(10000).then(function (res) {
	      return _this6.reloadTree();
	    }).then(function () {
	      _this6._selectedNodeId = nodeId;
	    }).catch(function (err) {
	      if (err) {
	        return _this6.handleEditError(err);
	      }
	    });
	  },
	  newNode: function newNode(parentNode) {
	    var _this7 = this;

	    if (!parentNode) {
	      return;
	    }
	    var parentId = parentNode.data.properties.id;

	    superagent.get(_webservice_urls2.default.getNewNodeUrl()).query({ name: this.state.treeId }).timeout(10000).end(function (err, res) {
	      if (err) {
	        return _this7.handleEditError(err);
	      }

	      var newNode = res.body.id;

	      superagent.get(_webservice_urls2.default.getNewEdgeUrl()).query({ name: _this7.state.treeId, src: parentId, dst: newNode }).end(function (err, res) {
	        if (err) {
	          return _this7.handleEditError(err);
	        }

	        _this7.reloadTree().then(function () {
	          _this7._selectedNodeId = newNode;
	        });
	      });
	    });
	  },
	  copyTree: function copyTree(id, newName) {
	    var _this8 = this;

	    if (!id || !newName) {
	      return;
	    }
	    return superagent.get(_webservice_urls2.default.getCopyTreeUrl()).query({ name: id, target: newName, type: 'tree' }).timeout(10000).then(function (res) {
	      _this8.fetchTreesList();

	      return res.text;
	    }).catch(function (err) {
	      _this8.handleTreesError(err);
	    });
	  },
	  newTree: function newTree(params) {
	    var _this9 = this;

	    if (!params || !params.name) {
	      return;
	    }
	    return superagent.get(_webservice_urls2.default.getNewTreeUrl()).query({ name: params.name, type: 'tree' }).timeout(10000).then(function (res) {
	      _this9.fetchTreesList();
	      return res.text;
	    }).catch(function (err) {
	      _this9.handleTreesError(err);
	    });
	  },
	  loadTrees: function loadTrees() {
	    if (!this.state.trees) {
	      this.fetchTreesList();
	    }
	  },
	  fetchTreesList: function fetchTreesList() {
	    var _this10 = this;

	    return superagent.get(_webservice_urls2.default.getTreeListUrl()).timeout(10000).then(function (res) {
	      var resTrees = res.body;

	      resTrees = resTrees.map(function (tree) {
	        tree.name = tree.graphName;
	        tree.description = 'Not edited';
	        return tree;
	      });
	      _this10.state.trees = resTrees;
	    }).catch(function (err) {
	      return _this10.handleTreesError(err);
	    });
	  },
	  handleTreeError: function handleTreeError(err, id) {
	    var message = 'Could not load Tree ' + id + '. ' + err.toString();
	    _appStateStore2.default.setMessage(message, true);
	  },
	  handleTreesError: function handleTreesError(err, id) {
	    var message = 'Could not load Tree List: ' + err.toString();
	    _appStateStore2.default.setMessage(message, true);
	  },
	  handleEditError: function handleEditError(err) {
	    var message = 'Could not edit tree: ' + err.toString();
	    _appStateStore2.default.setMessage(message, true);
	  },
	  filterTrees: function filterTrees(filterList) {
	    var filteredTrees = this.state.trees.filter(function (tree) {
	      return filterList.includes(tree.graphDBId);
	    });
	    return filteredTrees;
	  }
	};

/***/ },
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(_) {'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _webservice_urls = __webpack_require__(31);

	var _webservice_urls2 = _interopRequireDefault(_webservice_urls);

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var superagent = __webpack_require__(44);

	exports.default = {
	  state: {
	    chosenPatient: null,

	    patientTrees: [],

	    patientsJson: null,

	    patientNodeInfo: null
	  },
	  loadPatients: function loadPatients() {
	    if (!this.state.patientsJson) {
	      this.fetchPatientsList();
	    }
	  },
	  loadPatient: function loadPatient(id) {
	    var _this = this;

	    if (this.state.chosenPatient && id === this.state.chosenPatient.id) {
	      return;
	    }

	    this.clearPatient();
	    superagent.get(_webservice_urls2.default.getPatientUrl(id)).timeout(10000).end(function (err, res) {
	      if (err) {
	        return _this.handlePatientError(err, res);
	      }
	      var patient = res.body;
	      patient = _this.preprocessPatient(patient);

	      _this.state.chosenPatient = patient;
	      _this.updatePatientTrees();
	    });
	  },
	  preprocessPatient: function preprocessPatient(patient) {
	    var sortedActions = _.sortBy(patient.medicalActions, function (action) {
	      return action.timestamp;
	    });
	    var uniqueActions = _.uniqBy(sortedActions, function (action) {
	      return action.action.name;
	    });
	    patient.medicalActions = uniqueActions;

	    return patient;
	  },
	  clearPatient: function clearPatient() {
	    this.patientTrees = [];
	    this.state.patientNodeInfo = null;
	    this.state.chosenPatient = null;
	  },
	  updatePatientTrees: function updatePatientTrees() {
	    if (!this.state.chosenPatient) {
	      return;
	    }
	    var patientTreeNames = [];
	    this.state.chosenPatient.medicalActions.forEach(function (entry) {
	      patientTreeNames.push(entry.action.graphId);
	    });
	    this.state.patientTrees = patientTreeNames;
	  },
	  updatePatientNodeInfo: function updatePatientNodeInfo(nodeName) {
	    if (!this.state.chosenPatient) {
	      this.state.patientNodeInfo = null;
	      return;
	    }
	    var entry = this.state.chosenPatient.medicalActions.find(function (entry) {
	      return entry.action.name === nodeName;
	    });
	    if (!entry) {
	      this.state.patientNodeInfo = null;
	      return;
	    }

	    var nodeInfo = {};
	    nodeInfo['timestamp'] = entry.timestamp;
	    nodeInfo['result'] = entry.state;
	    nodeInfo['physician'] = {
	      id: entry.physician.id,
	      lastname: entry.physician.lastname,
	      firstname: entry.physician.firstname
	    };
	    this.state.patientNodeInfo = nodeInfo;
	  },
	  fetchPatientsList: function fetchPatientsList() {
	    var _this2 = this;

	    superagent.get(_webservice_urls2.default.getPatientListUrl()).timeout(10000).end(function (err, res) {
	      if (err) {
	        return _this2.handlePatientsError(err, res);
	      }
	      _this2.state.patientsJson = res.body;
	    });
	  },
	  handlePatientsError: function handlePatientsError(err, res) {
	    var message = 'Could not get Patient List. ' + err.toString();
	    _appStateStore2.default.setMessage(message, true);
	  },
	  handlePatientError: function handlePatientError(err, res) {
	    var message = 'Could not get Patient data. ' + err.toString();
	    _appStateStore2.default.setMessage(message, true);
	  },
	  handleError: function handleError(err, res) {
	    var message = 'Could not get Patient Data.' + err.toString();
	    _appStateStore2.default.setMessage(message, true);
	  }
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(30)))

/***/ },
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(153)

	/* script */
	__vue_exports__ = __webpack_require__(97)

	/* template */
	var __vue_template__ = __webpack_require__(275)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-54913da2"

	module.exports = __vue_exports__


/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _core = __webpack_require__(239);

	var _core2 = _interopRequireDefault(_core);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var lodashCustom = _core2.default.extend({}, _core2.default);
	lodashCustom.debounce = __webpack_require__(240);
	lodashCustom.uniqBy = __webpack_require__(253);

	module.exports = lodashCustom;

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var urls = {
	  getTreeUrl: function getTreeUrl() {
	    return 'http://10.200.1.75:8012/tree';
	  },
	  getTreeListUrl: function getTreeListUrl() {
	    return 'http://10.200.1.75:8016/graphs/all-trees';
	  },
	  getNodeDeleteUrl: function getNodeDeleteUrl(id) {
	    return 'http://10.200.1.75:8012/graph/vertex/remove/' + id;
	  },
	  getNodeUpdateUrl: function getNodeUpdateUrl(id) {
	    return 'http://10.200.1.75:8012/tree/vertex/edit/' + id;
	  },
	  getNewNodeUrl: function getNewNodeUrl() {
	    return 'http://10.200.1.75:8012/graph/vertex/add';
	  },
	  getNewEdgeUrl: function getNewEdgeUrl() {
	    return 'http://10.200.1.75:8012/graph/edge/add';
	  },
	  getDeleteEdgeUrl: function getDeleteEdgeUrl() {
	    return 'http://10.200.1.75:8012/graph/edge/remove';
	  },
	  getNewTreeUrl: function getNewTreeUrl() {
	    return 'http://10.200.1.75:8012/graph/add';
	  },
	  getCopyTreeUrl: function getCopyTreeUrl() {
	    return 'http://10.200.1.75:8012/graph/copy';
	  },
	  getPatientUrl: function getPatientUrl(id) {
	    return 'http://10.200.1.75:8016/patients/id/' + id;
	  },
	  getPatientListUrl: function getPatientListUrl() {
	    return 'http://10.200.1.75:8016/patients/all';
	  },
	  postLoginUrl: function postLoginUrl() {
	    return 'http://10.200.1.75:8016/users/login';
	  }
	};

	if (process && ({"NODE_ENV":"production"}).OFFLINE) {
	  console.warn('Using static fixtures for webservice calls!');
	  urls = {
	    getTreeUrl: function getTreeUrl(id) {
	      return '/static/tree/' + id + '.json';
	    },
	    getTreeListUrl: function getTreeListUrl() {
	      return '/static/graphs/all-trees.json';
	    },
	    getPatientUrl: function getPatientUrl(id) {
	      return '/static/patients/id/' + id + '.json';
	    },
	    getPatientListUrl: function getPatientListUrl() {
	      return '/static/patients/all.json';
	    },
	    postLoginUrl: function postLoginUrl() {
	      return '/static/users/login';
	    }
	  };
	}

	exports.default = urls;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(254)))

/***/ },
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(149)

	/* script */
	__vue_exports__ = __webpack_require__(93)

	/* template */
	var __vue_template__ = __webpack_require__(270)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-1e2b9dff"

	module.exports = __vue_exports__


/***/ },
/* 81 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(154)

	/* script */
	__vue_exports__ = __webpack_require__(95)

	/* template */
	var __vue_template__ = __webpack_require__(277)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-57a46ece"

	module.exports = __vue_exports__


/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(156)

	/* script */
	__vue_exports__ = __webpack_require__(96)

	/* template */
	var __vue_template__ = __webpack_require__(279)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-625de7a2"

	module.exports = __vue_exports__


/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(157)

	/* script */
	__vue_exports__ = __webpack_require__(99)

	/* template */
	var __vue_template__ = __webpack_require__(280)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

	module.exports = __vue_exports__


/***/ },
/* 84 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(_) {'use strict';

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var d3 = _.extend({}, __webpack_require__(147), __webpack_require__(60), __webpack_require__(142), __webpack_require__(145));


	var animationDuration = 500;
	var globalId = 1;
	function setUniqueId(d) {
	  return d._id || (d._id = globalId++);
	}

	function getShortName(d) {
	  var name = d.data.name;
	  if (name.length > 16) {
	    var spacePos = name.indexOf(' ', 7);
	    if (spacePos < 0 || spacePos > 13) {
	      name = name.slice(0, 11) + '...';
	    } else {
	      name = name.slice(0, spacePos);
	    }
	  }
	  return name;
	}

	function textPositionLeft(d) {
	  var left = true;
	  if (!d.parent) {
	    left = false;
	  }
	  return left;
	}

	module.exports = function (domId, data) {
	  var svg = d3.select(domId);
	  var width = +svg.attr('width');
	  var height = +svg.attr('height');
	  var g = svg.append('g').attr('transform', 'translate(25,0)');
	  var selectedNode = null;
	  var tree = d3.tree().size([height, width - 55]);

	  var originalData = null;

	  originalData = d3.hierarchy(data);

	  var nodeClass = function nodeClass(d) {
	    var classes = ['node'];

	    if (!d.parent) {
	      classes.push('node--left');
	    } else if (d.children) {
	      classes.push('node--internal');
	    } else {
	      classes.push('node--left');
	    }

	    if (d._children || (!d.children || d.children.size === 0) && d.data.properties['out-degree'] > 0) {
	      classes.push('hidden-children');
	    }

	    var type = d.data.type;
	    classes.push(type);

	    if (selectedNode) {
	      if (d._id === selectedNode._id) {
	        classes.push('selected');
	      }

	      if (d.children) {
	        var childrenIds = d.children.map(function (child) {
	          return child._id;
	        });
	        if (childrenIds.includes(selectedNode._id)) {
	          classes.push('selected-neighbor');
	        }
	      }

	      if (d.parent && d.parent._id === selectedNode._id) {
	        classes.push('selected-neighbor');
	      }

	      var id = selectedNode.data.properties['titan-db-id'];
	      if (d.data.properties['titan-db-id'] === id) {
	        classes.push('selected-twin');
	      }
	    }

	    var nodeInfo = d3Tree._nodeInfos[d.data.name];
	    if (nodeInfo) {
	      nodeInfo.medicalActionPositive ? classes.push('action-pos') : classes.push('action-neg');
	    } else {
	      classes.push('no-action');
	    }

	    return classes.join(' ');
	  };

	  var d3Tree = {
	    _nodeInfos: {},
	    update: function update(source) {
	      var root = originalData;
	      root.x0 = 250;
	      root.y0 = 0;
	      root.each(function (n) {
	        setUniqueId(n);
	        n._shortName = getShortName(n);
	      });

	      var link = g.selectAll('.link').data(tree(root).descendants().slice(1), function (d) {
	        return d._id;
	      });

	      link.transition().duration(animationDuration).attr('d', function (d) {
	        return 'M' + d.y + ',' + d.x + 'C' + (d.y + d.parent.y) / 2 + ',' + d.x + ' ' + (d.y + d.parent.y) / 2 + ',' + d.parent.x + ' ' + d.parent.y + ',' + d.parent.x;
	      });

	      var linkExit = link.exit().remove();

	      var linkEnter = link.enter().append('path').attr('class', 'link').attr('d', function (d) {
	        return 'M' + (source || d).y0 + ',' + (source || d).x0 + 'C' + ((source || d).y0 + (source || d.parent).y0) / 2 + ',' + (source || d).x0 + ' ' + ((source || d).y0 + (source || d.parent).y0) / 2 + ',' + (source || d.parent).x0 + ' ' + (source || d.parent).y0 + ',' + (source || d.parent).x0;
	      }).transition().duration(animationDuration).attr('d', function (d) {
	        return 'M' + d.y + ',' + d.x + 'C' + (d.y + d.parent.y) / 2 + ',' + d.x + ' ' + (d.y + d.parent.y) / 2 + ',' + d.parent.x + ' ' + d.parent.y + ',' + d.parent.x;
	      });

	      var node = g.selectAll('.node').data(tree(root).descendants(), function (d) {
	        return d._id;
	      }).attr('class', nodeClass);

	      var nodeUpdate = node.transition().duration(animationDuration).attr('transform', function (d) {
	        return 'translate(' + d.y + ',' + d.x + ')';
	      });

	      var nodeEnter = node.enter().append('g').attr('transform', function (d) {
	        return 'translate(' + (source || d).y0 + ',' + (source || d).x0 + ')';
	      }).attr('class', nodeClass).on('click', function (d, i) {
	        _treeStore2.default.selectNode(d);
	      });

	      nodeEnter.transition().duration(animationDuration).attr('transform', function (d) {
	        return 'translate(' + d.y + ',' + d.x + ')';
	      });

	      nodeEnter.append('circle').attr('r', 5);

	      nodeEnter.append('path').attr('class', 'triangle').attr('d', d3.symbol().size(20).type(d3.symbolTriangle)).attr('transform', 'translate(11,0) rotate(210)');

	      nodeEnter.append('text').attr('dy', -10).attr('x', function (d) {
	        return textPositionLeft(d) ? -10 : 10;
	      }).style('text-anchor', function (d) {
	        return textPositionLeft(d) ? 'end' : 'start';
	      }).attr('class', 'node-text').text(function (d) {
	        return d._shortName;
	      }).on('mouseover', function (d, i) {
	        d3.select(this).text(function (d) {
	          return d.data.name;
	        });
	      }).on('mouseout', function (d) {
	        d3.select(this).text(function (d) {
	          return d._shortName;
	        });
	      });

	      var nodeExit = node.exit().transition().duration(animationDuration).attr('transform', function (d) {
	        return 'translate(' + (source || d.parent).y + ',' + (source || d.parent).x + ')';
	      }).remove();
	      root.each(function (d) {
	        d.x0 = d.x;
	        d.y0 = d.y;
	      });
	    },
	    toggleChildNodes: function toggleChildNodes(node) {
	      if (node.children && node.children.length > 0) {
	        node._children = node.children;
	        node.children = null;
	      } else if (node._children) {
	        node.children = node._children;
	        node._children = null;
	      } else if (node.data.properties['out-degree'] > 0) {
	        _treeStore2.default.amendTree(node);
	      }
	      this.update(node);
	    },
	    setNodeInfos: function setNodeInfos(map) {
	      this._nodeInfos = map;
	      this.update();
	    },
	    setSelectedNode: function setSelectedNode(node) {
	      selectedNode = node;
	      this.update();
	    },
	    getNodeById: function getNodeById(id) {
	      var result;
	      originalData.each(function (node) {
	        if (node.data.properties.id === id) {
	          result = node;
	        }
	      });
	      return result;
	    },
	    clear: function clear() {
	      g.selectAll('*').remove();
	    }
	  };
	  d3Tree.update(originalData);
	  return d3Tree;
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(30)))

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _treeManager = __webpack_require__(83);

	var _treeManager2 = _interopRequireDefault(_treeManager);

	var _headerBar = __webpack_require__(266);

	var _headerBar2 = _interopRequireDefault(_headerBar);

	var _login = __webpack_require__(80);

	var _login2 = _interopRequireDefault(_login);

	var _flash = __webpack_require__(265);

	var _flash2 = _interopRequireDefault(_flash);

	var _selection = __webpack_require__(82);

	var _selection2 = _interopRequireDefault(_selection);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'app',
	  data: function data() {
	    return {
	      appState: _appStateStore2.default
	    };
	  },
	  computed: {
	    showNavbar: function showNavbar() {
	      return !!this.appState.state.user;
	    },
	    containerClass: function containerClass() {
	      var route = this.$route;
	      var fluid = route.name === 'tree' || route.name === 'patient';

	      return {
	        'container-fluid': fluid,
	        'container': !fluid
	      };
	    }
	  },
	  components: {
	    'header-bar': _headerBar2.default,
	    'tree-manager': _treeManager2.default,
	    'flash': _flash2.default,
	    'selection': _selection2.default,
	    'login': _login2.default
	  }
	};

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _circlePanel = __webpack_require__(262);

	var _circlePanel2 = _interopRequireDefault(_circlePanel);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'circlepanelmanager',
	  components: {
	    'circlepanel': _circlePanel2.default
	  },
	  data: function data() {
	    return {
	      treeState: _treeStore2.default.state,
	      openNodes: []
	    };
	  },
	  computed: {
	    storeNode: function storeNode() {
	      return this.treeState.menuNode;
	    },
	    moving: function moving() {
	      return this.treeState.moving;
	    }
	  },
	  methods: {
	    startMove: function startMove(params) {
	      _treeStore2.default.startMoving(params);
	    },
	    cancelMove: function cancelMove() {
	      _treeStore2.default.cancelMove();
	    }
	  },
	  watch: {
	    storeNode: function storeNode(newNode) {
	      this.openNodes = [];
	      if (newNode) {
	        this.openNodes.push(newNode);
	      }
	    }
	  }
	};

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _confirmationDialog = __webpack_require__(263);

	var _confirmationDialog2 = _interopRequireDefault(_confirmationDialog);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  components: {
	    'confirmation': _confirmationDialog2.default
	  },
	  name: 'circlepanel',
	  data: function data() {
	    return {
	      treeStore: _treeStore2.default,
	      confirmationTitle: ''
	    };
	  },
	  props: {
	    node: {
	      type: Object,
	      required: true
	    }
	  },
	  computed: {
	    top: function top() {
	      return this.node.x - 7;
	    },
	    left: function left() {
	      return this.node.y + 25;
	    }
	  },
	  methods: {
	    toggle: function toggle() {
	      this.treeStore.toggleChildNodes(this.node);
	    },
	    deselect: function deselect() {
	      this.treeStore.selectNode(this.node);
	    },
	    newNode: function newNode() {
	      this.treeStore.newNode(this.node);
	      this.deselect();
	    },
	    clickDelete: function clickDelete() {
	      this.confirmationTitle = 'Delete Node?';
	    },
	    deleteNode: function deleteNode() {
	      this.treeStore.deleteNode(this.node);
	      this.deselect();
	    },
	    moveNode: function moveNode() {
	      var moveNodeId = this.node.data.properties.id;
	      var moveNodeParent = this.node.parent.data.properties.id;
	      var params = { moveNode: moveNodeId, oldParent: moveNodeParent };
	      this.$emit('move', params);
	    },
	    cancelAction: function cancelAction() {
	      this.confirmationTitle = '';
	    }
	  }
	};

/***/ },
/* 88 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'confirmation-dialog',
	  data: function data() {
	    return {
	      visible: false
	    };
	  },
	  props: {
	    title: String,
	    type: String
	  },
	  computed: {
	    buttonClass: function buttonClass() {
	      return {
	        'btn-primary': this.type !== 'alert',
	        'btn-danger': this.type === 'alert'
	      };
	    },
	    headerClass: function headerClass() {
	      return {
	        'alert-danger': this.type === 'alert'
	      };
	    }
	  },
	  mounted: function mounted() {
	    var _this = this;

	    setTimeout(function () {
	      _this.visible = true;
	    });
	  },

	  methods: {
	    cancel: function cancel() {
	      this.$emit('cancel');
	    },
	    confirmIt: function confirmIt() {
	      this.$emit('confirm');
	    }
	  }
	};

/***/ },
/* 89 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'error-page',
	  components: {},
	  data: function data() {
	    return {};
	  },
	  computed: {}
	};

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'flash',
	  data: function data() {
	    return {
	      appState: _appStateStore2.default,
	      messages: _appStateStore2.default.state.messages
	    };
	  },
	  props: {},
	  computed: {},
	  methods: {
	    alertType: function alertType(message) {
	      return { 'alert-info': !message.error, 'alert-danger': message.error };
	    }
	  }
	};

/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _patientStore = __webpack_require__(7);

	var _patientStore2 = _interopRequireDefault(_patientStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'header-bar',
	  data: function data() {
	    return {
	      treeStore: _treeStore2.default.state,
	      patientStore: _patientStore2.default.state,
	      appState: _appStateStore2.default
	    };
	  },
	  computed: {
	    tree: function tree() {
	      if (this.treeStore.tree) {
	        return this.treeStore.tree;
	      }
	      return this.treeStore.tree;
	    },
	    patient: function patient() {
	      return this.patientStore.chosenPatient;
	    },
	    pageTitle: function pageTitle() {
	      if (this.tree) {
	        return this.tree.name;
	      }
	      return 'Selection';
	    },
	    subTitle: function subTitle() {
	      if (!this.tree) {
	        return 'Chose a Tree or a Patient';
	      }
	      if (this.patient) {
	        return 'Patient: ' + this.patient.firstname + ' ' + this.patient.lastname;
	      }
	      return 'not edited';
	    }
	  },
	  methods: {
	    logout: function logout() {
	      this.appState.logout();
	      if (!this.appState.state.user) {
	        this.$router.push('login');
	      }
	    },
	    resetSelection: function resetSelection() {
	      this.appState.resetSelection();
	      this.$router.push('/');
	    }
	  }
	};

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _patientStore = __webpack_require__(7);

	var _patientStore2 = _interopRequireDefault(_patientStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'infopanel',
	  data: function data() {
	    return {
	      editing: false,
	      newProps: null,
	      treeState: _treeStore2.default.state,
	      patientState: _patientStore2.default.state,
	      types: [{ text: 'Symptom', value: 'symptom' }, { text: 'Diagnosis', value: 'diagnosis' }, { text: 'Therapy', value: 'therapy' }, { text: 'Examination', value: 'examination' }]
	    };
	  },
	  computed: {
	    infoNode: function infoNode() {
	      return this.treeState.infoNode;
	    },
	    patient: function patient() {
	      return this.patientState.chosenPatient;
	    },
	    nodeType: function nodeType() {
	      var type = this.infoNode.data.type;
	      return type.replace(/^./, function (char) {
	        return char.toUpperCase();
	      });
	    },
	    patientNodeInfo: function patientNodeInfo() {
	      return this.patientState.patientNodeInfo;
	    },
	    physicianName: function physicianName() {
	      var doctor = this.patientState.patientNodeInfo.physician;
	      return doctor.lastname + ', ' + doctor.firstname;
	    },
	    medicalActionDate: function medicalActionDate() {
	      var date = new Date(Number(this.patientNodeInfo.timestamp));
	      return date.toDateString();
	    },
	    pos: function pos() {
	      if (this.patientNodeInfo) {
	        var res = this.patientNodeInfo.result;
	        return res === 'POSITIVE';
	      }
	      return false;
	    },
	    neg: function neg() {
	      if (this.patientNodeInfo) {
	        var res = this.patientNodeInfo.result;
	        return res === 'NEGATIVE';
	      }
	      return false;
	    },
	    nodeTypeClass: function nodeTypeClass() {
	      var icon;
	      var type = this.infoNode.data.type.toLowerCase();
	      switch (type) {
	        case 'diagnosis':
	          icon = 'lightbulb-o';
	          break;
	        case 'therapy':
	          icon = 'hospital-o';
	          break;
	        case 'symptom':
	          icon = 'tint';
	          break;
	        case 'examination':
	          icon = 'stethoscope';
	          break;
	        default:
	          icon = 'question';
	      }
	      return 'fa fa-' + icon;
	    }
	  },
	  methods: {
	    editNode: function editNode() {
	      this.editing = true;
	      this.newProps = {
	        name: this.infoNode.data.name,
	        type: this.infoNode.data.type,
	        description: this.infoNode.data.properties.description,

	        id: this.infoNode.data.properties.id
	      };
	      var root = this.infoNode.data.properties.root;
	      if (root) {
	        this.newProps.root = root;
	      };
	      this.newProps['titan-db-id'] = this.infoNode.data.properties['titan-db-id'];
	    },
	    cancelEdit: function cancelEdit() {
	      this.editing = false;
	      this.newProps = null;
	    },
	    saveNode: function saveNode() {
	      this.editing = false;

	      var nodeId = this.newProps.id;
	      _treeStore2.default.updateNode(nodeId, this.newProps);
	    }
	  }
	};

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _spinner = __webpack_require__(29);

	var _spinner2 = _interopRequireDefault(_spinner);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'login',
	  components: {
	    'spinner': _spinner2.default
	  },
	  data: function data() {
	    return {
	      appState: _appStateStore2.default,
	      username: '',
	      password: ''
	    };
	  },
	  computed: {
	    buttonDisabled: function buttonDisabled() {
	      return !(this.username !== '' & this.password !== '' & !this.appState.state.loggingIn);
	    },
	    user: function user() {
	      return this.appState.state.user;
	    }
	  },
	  beforeMount: function beforeMount() {
	    if (this.user) {
	      this.$router.push('/');
	    }
	  },
	  watch: {
	    user: function user(newUser) {
	      this.$router.push('/');
	    }
	  },
	  methods: {
	    login: function login() {
	      if (this.username !== '' & this.password !== '') {
	        this.appState.login(this.username, this.password);
	      }
	    }
	  }
	};

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'patientcreator',
	  components: {},
	  data: function data() {
	    return {
	      treeState: _treeStore2.default.state
	    };
	  },
	  computed: {},
	  methods: {},
	  watch: {}
	};

/***/ },
/* 95 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'selection-list',
	  props: {
	    newButton: Boolean,
	    heading: String,
	    activeItem: {
	      type: Object,
	      default: null
	    },
	    items: Array
	  },
	  data: function data() {
	    return {
	      searchText: null
	    };
	  },
	  methods: {
	    itemSelected: function itemSelected(item) {
	      this.$emit('itemselected', item);
	    },
	    newElement: function newElement() {
	      this.$emit('new');
	    }
	  },
	  computed: {
	    displayItems: function displayItems() {
	      var search = this.searchText;
	      if (!search || search.length < 3) {
	        return this.items;
	      };
	      search = search.toLowerCase();
	      var filtered = this.items.filter(function (item) {
	        return item.name.toLowerCase().includes(search) || item.description.toLowerCase().includes(search);
	      });
	      return filtered;
	    }
	  },
	  watch: {}
	};

/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _patientStore = __webpack_require__(7);

	var _patientStore2 = _interopRequireDefault(_patientStore);

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _selectionList = __webpack_require__(81);

	var _selectionList2 = _interopRequireDefault(_selectionList);

	var _spinner = __webpack_require__(29);

	var _spinner2 = _interopRequireDefault(_spinner);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'selection',
	  components: {
	    'spinner': _spinner2.default,
	    'selection-list': _selectionList2.default
	  },
	  data: function data() {
	    return {
	      treeStore: _treeStore2.default,
	      patientStore: _patientStore2.default,
	      selectedPatient: null,
	      selectedTree: null,
	      appState: _appStateStore2.default
	    };
	  },
	  computed: {
	    patients: function patients() {
	      var patientsRaw = this.patientStore.state.patientsJson;
	      if (!patientsRaw) {
	        return null;
	      }
	      return patientsRaw.map(function (patient) {
	        patient.name = patient.firstname + ' ' + patient.lastname;
	        var date = new Date(Number(patient.dateOfBirth)).toDateString();
	        var gender = patient.gender.toLowerCase();
	        var id = patient.id;
	        patient.description = 'id: ' + id + '  |  *' + date + '  |  ' + gender;
	        return patient;
	      });
	    },
	    tree: function tree() {
	      return this.treeStore.state.tree;
	    },
	    treeListHeading: function treeListHeading() {
	      if (this.selectedPatient) {
	        return 'Trees with Data for this Patient:';
	      }
	      return 'Choose a Tree:';
	    },
	    trees: function trees() {
	      var rawTrees;
	      if (this.patientStore.state.chosenPatient) {
	        rawTrees = this.treeStore.filterTrees(this.patientStore.state.patientTrees);
	      } else {
	        rawTrees = this.treeStore.state.trees;
	      }
	      if (!rawTrees) {
	        return null;
	      }
	      return rawTrees;
	    }
	  },
	  methods: {
	    newTree: function newTree() {
	      this.$router.push({ name: 'newTree' });
	    },
	    newPatient: function newPatient() {
	      this.$router.push({ name: 'newPatient' });
	    },

	    patientSelected: function patientSelected(patient) {
	      if (this.selectedPatient === patient) {
	        this.selectedPatient = null;
	        this.patientStore.clearPatient();
	        return;
	      }
	      this.selectedPatient = patient;
	      if (patient.id) {
	        this.patientStore.loadPatient(patient.id);
	      }
	    },
	    treeSelected: function treeSelected(tree) {
	      this.selectedTree = tree;
	      if (tree.graphDBId) {
	        this.treeStore.loadTree(tree.graphDBId);
	      }
	    }
	  },
	  watch: {
	    tree: function tree(newTree) {
	      var id = this.treeStore.state.treeId;
	      var patient = this.patientStore.state.chosenPatient;
	      if (!newTree || !id) {
	        this.appState.setMessage('Error: Tree was not loaded completely.', true);
	        return;
	      }

	      if (patient) {
	        this.$router.push({ name: 'patient', params: { treeId: id, patientId: patient.id } });
	        return;
	      }

	      this.$router.push({ name: 'tree', params: { treeId: id } });
	    }
	  },
	  beforeMount: function beforeMount() {
	    if (!this.appState.state.user) {
	      this.$router.replace('/');
	    }
	    this.patientStore.loadPatients();
	    this.treeStore.loadTrees();
	  }
	};

/***/ },
/* 97 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  name: 'spinner',
	  props: {},
	  data: function data() {
	    return {};
	  }
	};

/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _selectionList = __webpack_require__(81);

	var _selectionList2 = _interopRequireDefault(_selectionList);

	var _spinner = __webpack_require__(29);

	var _spinner2 = _interopRequireDefault(_spinner);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'treecreator',
	  components: {
	    'selection-list': _selectionList2.default,
	    'spinner': _spinner2.default
	  },
	  data: function data() {
	    return {
	      appState: _appStateStore2.default,
	      treeState: _treeStore2.default.state,
	      selectedTree: null,
	      newTreeData: { name: '' },
	      copyTreeData: { id: null, name: '' },
	      creating: false
	    };
	  },
	  computed: {
	    trees: function trees() {
	      return this.treeState.trees;
	    },

	    treeListHeading: function treeListHeading() {
	      return 'Choose a Tree you want to Copy:';
	    }
	  },
	  mounted: function mounted() {
	    if (this.treeState.trees) return;
	    _treeStore2.default.loadTrees();
	  },

	  methods: {
	    treeSelected: function treeSelected(tree) {
	      if (!tree) return;
	      if (tree === this.selectedTree) {
	        this.selectedTree = null;
	        this.copyTreeData.name = '';
	        return;
	      }
	      this.selectedTree = tree;
	      var treeId = tree.graphDBId;
	      if (treeId) {
	        this.copyTreeData.id = treeId;
	        var newName = tree.name + '_copy';
	        this.copyTreeData.name = newName;
	      }
	    },
	    copyTree: function copyTree() {
	      var _this = this;

	      if (!this.copyTreeData.name || !this.copyTreeData.id) return;
	      this.creating = true;
	      _treeStore2.default.copyTree(this.copyTreeData.id, this.copyTreeData.name).then(function (newId) {
	        _this.creating = false;
	        _this.$router.push({ name: 'tree', params: { treeId: newId } });
	      }).catch(function (err) {
	        _this.creating = false;
	        var message = 'Could not copy Tree: ' + err.toString();
	        _this.appState.setMessage(message, true);
	      });
	    },
	    newTree: function newTree() {
	      var _this2 = this;

	      if (!this.newTreeData || !this.newTreeData.name) return;
	      this.creating = true;
	      _treeStore2.default.newTree(this.newTreeData).then(function (newId) {
	        _this2.creating = false;

	        _this2.$router.push({ name: 'tree', params: { treeId: newId } });
	      }).catch(function (err) {
	        _this2.creating = false;
	        var message = 'Could not create Tree: ' + err.toString();
	        _this2.appState.setMessage(message, true);
	      });
	    }
	  },
	  watch: {}
	};

/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($, _) {'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _appStateStore = __webpack_require__(2);

	var _appStateStore2 = _interopRequireDefault(_appStateStore);

	var _treeStore = __webpack_require__(3);

	var _treeStore2 = _interopRequireDefault(_treeStore);

	var _patientStore = __webpack_require__(7);

	var _patientStore2 = _interopRequireDefault(_patientStore);

	var _circlePanelManager = __webpack_require__(261);

	var _circlePanelManager2 = _interopRequireDefault(_circlePanelManager);

	var _infopanel = __webpack_require__(267);

	var _infopanel2 = _interopRequireDefault(_infopanel);

	var _spinner = __webpack_require__(29);

	var _spinner2 = _interopRequireDefault(_spinner);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = {
	  name: 'treemanager',
	  components: {
	    'spinner': _spinner2.default,
	    'circlepanelmanager': _circlePanelManager2.default,
	    'infopanel': _infopanel2.default
	  },
	  data: function data() {
	    return {
	      treeState: _treeStore2.default.state,
	      patientState: _patientStore2.default.state,
	      appState: _appStateStore2.default.state,
	      svgWidth: 960,
	      infopanelCollapsed: false
	    };
	  },
	  mounted: function mounted() {
	    this.checkURL();

	    this.resizeTree();

	    $(window).on('resize', _.debounce(this.resizeTree, 500).bind(this));
	  },
	  beforeDestroy: function beforeDestroy() {
	    _patientStore2.default.clearPatient();
	    _treeStore2.default.clearTree();
	  },
	  computed: {
	    patient: function patient() {
	      return this.patientState.chosenPatient;
	    },
	    tree: function tree() {
	      var json = this.treeState.tree;
	      if (!json) {
	        return null;
	      }
	      return json;
	    },
	    isPatientChosen: function isPatientChosen() {
	      return !!this.patientState.chosenPatient;
	    },
	    svgColSize: function svgColSize() {
	      return this.infopanelCollapsed ? 'col-md-12' : 'col-md-9';
	    },
	    infoColSize: function infoColSize() {
	      return this.infopanelCollapsed ? 'col-md-12' : 'col-md-9';
	    }
	  },
	  watch: {
	    '$route': function $route(to, from) {
	      this.checkURL();
	    },
	    tree: function tree(newTree) {
	      this.constructD3Tree();
	      this.calculateNodeInfos();
	    },
	    patient: function patient(newPatient) {
	      this.calculateNodeInfos();
	    }
	  },
	  methods: {
	    toggleInfopanel: function toggleInfopanel() {
	      var _this = this;

	      this.infopanelCollapsed = !this.infopanelCollapsed;
	      this.$nextTick(function () {
	        _this.resizeTree();
	      });
	    },
	    resizeTree: function resizeTree() {
	      var _this2 = this;

	      var svgContainer = $('.svg-container', this.$el);
	      this.svgWidth = svgContainer.width();

	      this.$nextTick(function () {
	        _this2.constructD3Tree();
	        _this2.calculateNodeInfos();
	      });
	    },

	    calculateNodeInfos: function calculateNodeInfos() {
	      var patient = this.patientState.chosenPatient;
	      var treeId = this.treeState.treeId;
	      var nodeInfos = {};
	      if (patient && treeId) {
	        patient.medicalActions.forEach(function (action) {
	          var resultVal = action.state === 'POSITIVE';
	          nodeInfos[action.action.name] = {
	            medicalActionPositive: resultVal
	          };
	        });
	      }
	      _treeStore2.default.setNodeInfos(nodeInfos);
	      return nodeInfos;
	    },
	    constructD3Tree: function constructD3Tree() {
	      if (!this.tree) {
	        return;
	      }
	      var treeConstructor = __webpack_require__(84);
	      var D3tree = treeConstructor('.tree', this.tree);
	      _treeStore2.default.setD3Tree(D3tree);
	    },

	    checkURL: function checkURL() {
	      var treeId = this.treeState.treeId;
	      var loadedTree = this.treeState.tree;

	      if (this.$route.params.patientId) {
	        if (!this.patientState.chosenPatient || this.patientState.chosenPatient.id !== this.$route.params.patientId) {
	          _patientStore2.default.loadPatient(this.$route.params.patientId);
	        }
	      }

	      if (loadedTree && treeId === this.$route.params.treeId) {
	        return;
	      };

	      _treeStore2.default.loadTree(this.$route.params.treeId);
	    }
	  }
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(162), __webpack_require__(30)))

/***/ },
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 149 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 150 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 151 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 152 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 153 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 154 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 155 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 156 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 157 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 158 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 159 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 160 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 161 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* script */
	__vue_exports__ = __webpack_require__(85)

	/* template */
	var __vue_template__ = __webpack_require__(276)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

	module.exports = __vue_exports__


/***/ },
/* 261 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(155)

	/* script */
	__vue_exports__ = __webpack_require__(86)

	/* template */
	var __vue_template__ = __webpack_require__(278)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-5c789d81"

	module.exports = __vue_exports__


/***/ },
/* 262 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(160)

	/* script */
	__vue_exports__ = __webpack_require__(87)

	/* template */
	var __vue_template__ = __webpack_require__(283)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-7eb833a1"

	module.exports = __vue_exports__


/***/ },
/* 263 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(159)

	/* script */
	__vue_exports__ = __webpack_require__(88)

	/* template */
	var __vue_template__ = __webpack_require__(282)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-746ee714"

	module.exports = __vue_exports__


/***/ },
/* 264 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(158)

	/* script */
	__vue_exports__ = __webpack_require__(89)

	/* template */
	var __vue_template__ = __webpack_require__(281)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-745313e4"

	module.exports = __vue_exports__


/***/ },
/* 265 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(151)

	/* script */
	__vue_exports__ = __webpack_require__(90)

	/* template */
	var __vue_template__ = __webpack_require__(273)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-40440306"

	module.exports = __vue_exports__


/***/ },
/* 266 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(150)

	/* script */
	__vue_exports__ = __webpack_require__(91)

	/* template */
	var __vue_template__ = __webpack_require__(271)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-1f5cc7a6"

	module.exports = __vue_exports__


/***/ },
/* 267 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* script */
	__vue_exports__ = __webpack_require__(92)

	/* template */
	var __vue_template__ = __webpack_require__(272)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

	module.exports = __vue_exports__


/***/ },
/* 268 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(152)

	/* script */
	__vue_exports__ = __webpack_require__(94)

	/* template */
	var __vue_template__ = __webpack_require__(274)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-433eacfa"

	module.exports = __vue_exports__


/***/ },
/* 269 */
/***/ function(module, exports, __webpack_require__) {

	var __vue_exports__, __vue_options__

	/* styles */
	__webpack_require__(161)

	/* script */
	__vue_exports__ = __webpack_require__(98)

	/* template */
	var __vue_template__ = __webpack_require__(284)
	__vue_options__ = __vue_exports__ = __vue_exports__ || {}
	if (
	  typeof __vue_exports__.default === "object" ||
	  typeof __vue_exports__.default === "function"
	) {
	__vue_options__ = __vue_exports__ = __vue_exports__.default
	}
	if (typeof __vue_options__ === "function") {
	  __vue_options__ = __vue_options__.options
	}

	__vue_options__.render = __vue_template__.render
	__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
	__vue_options__._scopeId = "data-v-7ff4fa52"

	module.exports = __vue_exports__


/***/ },
/* 270 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('form', {
	    staticClass: "form-horizontal top-space",
	    attrs: {
	      "onsubmit": "login()"
	    }
	  }, [_h('div', {
	    staticClass: "form-group"
	  }, [_m(0), " ", _h('div', {
	    staticClass: "col-sm-4"
	  }, [_h('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (username),
	      expression: "username"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "text",
	      "id": "inputName",
	      "placeholder": "Your Name"
	    },
	    domProps: {
	      "value": _s(username)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        username = $event.target.value
	      }
	    }
	  })])]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_m(1), " ", _h('div', {
	    staticClass: "col-sm-4"
	  }, [_h('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (password),
	      expression: "password"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "password",
	      "id": "inputPassword",
	      "placeholder": "Password"
	    },
	    domProps: {
	      "value": _s(password)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        password = $event.target.value
	      }
	    }
	  })])]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_h('div', {
	    staticClass: "col-sm-offset-4 col-sm-10"
	  }, [_h('button', {
	    staticClass: "btn btn-primary btn-big",
	    attrs: {
	      "disabled": buttonDisabled,
	      "type": "submit"
	    },
	    on: {
	      "click": function($event) {
	        login()
	      }
	    }
	  }, ["Login"])])]), " ", (appState.state.loggingIn) ? _h('div', {
	    staticClass: "col-sm-offset-4 col-sm-4"
	  }, [_h('spinner')]) : _e()])
	}},staticRenderFns: [function (){with(this) {
	  return _h('label', {
	    staticClass: "col-sm-4 control-label",
	    attrs: {
	      "for": "inputName"
	    }
	  }, ["Username"])
	}},function (){with(this) {
	  return _h('label', {
	    staticClass: "col-sm-4 control-label",
	    attrs: {
	      "for": "inputPassword"
	    }
	  }, ["Password"])
	}}]}

/***/ },
/* 271 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('nav', {
	    staticClass: "navbar navbar-default"
	  }, [_h('div', {
	    staticClass: "container-fluid"
	  }, [_h('div', {
	    staticClass: "collapse navbar-collapse",
	    attrs: {
	      "id": "bs-example-navbar-collapse-1"
	    }
	  }, [_h('ul', {
	    staticClass: "nav navbar-nav"
	  }, [_h('li', [_h('a', {
	    attrs: {
	      "href": "#"
	    },
	    on: {
	      "click": function($event) {
	        resetSelection()
	      }
	    }
	  }, [_m(0)])]), _m(1)]), " ", _h('h4', {
	    staticClass: "navbar-text pad-left"
	  }, [_s(pageTitle) + "\n        ", _h('small', [_s(subTitle)])]), " ", _h('ul', {
	    staticClass: "nav navbar-nav navbar-right"
	  }, [_h('li', [_h('a', {
	    attrs: {
	      "href": "#"
	    }
	  }, [_s(appState.state.user.name)])]), " ", _h('li', [_h('a', {
	    on: {
	      "click": function($event) {
	        logout()
	      }
	    }
	  }, [_m(2)])])])])])])
	}},staticRenderFns: [function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-2x fa-home"
	  })
	}},function (){with(this) {
	  return _h('li')
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-2x fa-sign-out"
	  })
	}}]}

/***/ },
/* 272 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return (infoNode) ? _h('div', {
	    staticClass: "panel panel-default"
	  }, [_h('div', {
	    staticClass: "panel-heading"
	  }, [(editing) ? _h('h3', [_h('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (newProps.name),
	      expression: "newProps.name"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "text"
	    },
	    domProps: {
	      "value": _s(newProps.name)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        newProps.name = $event.target.value
	      }
	    }
	  })]) : _h('h3', {
	    staticClass: "panel-title"
	  }, ["\n      " + _s(infoNode.data.name) + "\n      ", _h('a', {
	    staticClass: "pull-right",
	    on: {
	      "click": function($event) {
	        $emit('toggle')
	      }
	    }
	  }, [_h('i', {
	    staticClass: "fa fa-chevron-right"
	  })])]), " "]), " ", _h('div', {
	    staticClass: "panel-body"
	  }, [(editing) ? _h('div', [_h('div', {
	    staticClass: "form-group"
	  }, [_h('select', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (newProps.type),
	      expression: "newProps.type"
	    }],
	    staticClass: "form-control",
	    on: {
	      "change": function($event) {
	        newProps.type = Array.prototype.filter.call($event.target.options, function(o) {
	          return o.selected
	        }).map(function(o) {
	          return "_value" in o ? o._value : o.value
	        })[0]
	      }
	    }
	  }, [_l((types), function(type) {
	    return _h('option', {
	      domProps: {
	        "value": type.value
	      }
	    }, ["\n          " + _s(type.text) + "\n          "])
	  })])]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_h('textarea', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (newProps.description),
	      expression: "newProps.description"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "text"
	    },
	    domProps: {
	      "value": _s(newProps.description)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        newProps.description = $event.target.value
	      }
	    }
	  })]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_h('div', {
	    staticClass: "row"
	  }, [_h('div', {
	    staticClass: "col-md-6"
	  }, [_h('a', {
	    staticClass: "btn btn-default form-control",
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        cancelEdit($event)
	      }
	    }
	  }, [_m(0), " Cancel"])]), " ", _h('div', {
	    staticClass: "col-md-6"
	  }, [_h('a', {
	    staticClass: "btn btn-primary form-control",
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        saveNode($event)
	      }
	    }
	  }, [_m(1), " Save"])])])])]) : _e(), " ", (!editing) ? _h('div', [_h('h5', [_h('i', {
	    class: nodeTypeClass
	  }), " " + _s(nodeType) + "\n      "]), " ", _h('p', [_s(infoNode.data.properties.description)]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_h('a', {
	    staticClass: "btn btn-default form-control",
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        editNode($event)
	      }
	    }
	  }, [_m(2), " Edit"])])]) : _e(), " ", (patient && patientNodeInfo) ? _h('ul', {
	    staticClass: "list-group"
	  }, [_h('li', {
	    staticClass: "list-group-item",
	    class: {
	      'text-danger': neg, 'text-success': pos
	    }
	  }, [_m(3), " Result: " + _s(patientNodeInfo.result) + " "]), " ", _h('li', {
	    staticClass: "list-group-item"
	  }, [_m(4), " Physician: " + _s(physicianName) + " "]), " ", _h('li', {
	    staticClass: "list-group-item"
	  }, [_m(5), " Date: " + _s(medicalActionDate) + " "])]) : _h('ul', {
	    staticClass: "list-group"
	  }, [(infoNode.data.properties.source) ? _h('li', {
	    staticClass: "list-group-item"
	  }, [_h('i', {
	    staticClass: "fa fa-book"
	  }), " Source: " + _s(infoNode.data.properties.source)]) : _e(), " ", (infoNode.data.properties.prob) ? _h('li', {
	    staticClass: "list-group-item"
	  }, [_h('i', {
	    staticClass: "fa fa-percent"
	  }), " Probability: " + _s(infoNode.data.properties.prob) + "\n      "]) : _e(), " ", _h('li', {
	    staticClass: "list-group-item"
	  }, [_h('i', {
	    staticClass: "fa fa-money"
	  }), " Value: " + _s(infoNode.data.value)]), " ", _h('li', {
	    staticClass: "list-group-item"
	  }, [_h('i', {
	    staticClass: "fa fa-level-up"
	  }), " Level: " + _s(infoNode.data.level)]), " ", _h('li', {
	    staticClass: "list-group-item"
	  }, [_h('i', {
	    staticClass: "fa fa-backward"
	  }), " Previous Node: " + _s(infoNode.data.parent)]), " ", _h('li', {
	    staticClass: "list-group-item"
	  }, [_h('i', {
	    staticClass: "fa fa-child"
	  }), " Has Children: " + _s(infoNode.data.children.length ? 'Yes' : 'No')])]), " "])]) : _e()
	}},staticRenderFns: [function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-times"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-save"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-edit"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-dot-circle-o"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-user-md"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-calendar"
	  })
	}}]}

/***/ },
/* 273 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', [_l((appState.state.messages), function(message) {
	    return _h('div', {
	      staticClass: "alert",
	      class: alertType(message),
	      attrs: {
	        "role": "alert"
	      }
	    }, [_s(message.text)])
	  })])
	}},staticRenderFns: []}

/***/ },
/* 274 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _m(0)
	}},staticRenderFns: [function (){with(this) {
	  return _h('div', ["\n  Hier wird einer neuer Patient erzeugt\n"])
	}}]}

/***/ },
/* 275 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _m(0)
	}},staticRenderFns: [function (){with(this) {
	  return _h('div', [_h('i', {
	    staticClass: "fa fa-refresh fa-spin fa-fw fa-5x spinner"
	  })])
	}}]}

/***/ },
/* 276 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', [(showNavbar) ? _h('header-bar') : _e(), " ", _h('div', {
	    class: containerClass
	  }, [_h('div', {
	    staticClass: "row"
	  }, [_h('flash'), " ", _h('router-view')])])])
	}},staticRenderFns: []}

/***/ },
/* 277 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', {
	    staticClass: "panel panel-default"
	  }, [_h('div', {
	    staticClass: "panel-heading"
	  }, [_h('h4', [_h('span', {
	    staticClass: "heading-text"
	  }, [_s(heading)]), " ", (newButton) ? _h('a', {
	    staticClass: "btn btn-success",
	    attrs: {
	      "style": "float: right; line-height: 1.5em;"
	    },
	    on: {
	      "click": newElement
	    }
	  }, [_m(0), " New"]) : _e()])]), " ", _h('div', {
	    staticClass: "panel-body"
	  }, [_h('div', {
	    staticClass: "input-group"
	  }, [_m(1), " ", _h('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (searchText),
	      expression: "searchText"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "text",
	      "placeholder": "Search..."
	    },
	    domProps: {
	      "value": _s(searchText)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        searchText = $event.target.value
	      }
	    }
	  })]), " ", _h('div', {
	    staticClass: "list-group"
	  }, [_l((displayItems), function(item) {
	    return _h('a', {
	      staticClass: "list-group-item",
	      class: {
	        active: item === activeItem
	      },
	      attrs: {
	        "href": "#"
	      },
	      on: {
	        "click": function($event) {
	          $event.preventDefault();
	          itemSelected(item)
	        }
	      }
	    }, [_h('h4', {
	      staticClass: "list-group-item-heading"
	    }, [_s(item.name)]), " ", _h('p', {
	      staticClass: "list-group-item-text"
	    }, [_s(item.description)])])
	  })])])])
	}},staticRenderFns: [function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-plus"
	  })
	}},function (){with(this) {
	  return _h('span', {
	    staticClass: "input-group-addon"
	  }, [_h('i', {
	    staticClass: "fa fa-search fa-fw"
	  })])
	}}]}

/***/ },
/* 278 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', [_l((openNodes), function(node) {
	    return (!moving) ? _h('circlepanel', {
	      key: node._id,
	      attrs: {
	        "node": node
	      },
	      on: {
	        "move": function($event) {
	          startMove($event)
	        }
	      }
	    }) : _e()
	  }), " ", (moving) ? _h('div', {
	    staticClass: "alert alert-info"
	  }, [_h('p', {
	    staticClass: "move-alert"
	  }, ["\n      Select the new Parent for the node you are moving!\n      ", _h('a', {
	    staticClass: "btn btn-default",
	    on: {
	      "click": cancelMove
	    }
	  }, [_m(0), " Cancel"])])]) : _e()])
	}},staticRenderFns: [function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-times"
	  })
	}}]}

/***/ },
/* 279 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', {
	    staticClass: "spaced"
	  }, [_h('div', {
	    staticClass: "col col-md-6"
	  }, [(patients) ? _h('selection-list', {
	    attrs: {
	      "newButton": false,
	      "activeItem": selectedPatient,
	      "heading": "Choose a Patient:",
	      "items": patients
	    },
	    on: {
	      "new": function($event) {
	        newPatient()
	      },
	      "itemselected": patientSelected
	    }
	  }) : _h('spinner'), " "]), " ", _h('div', {
	    staticClass: "col col-md-6"
	  }, [(trees) ? _h('selection-list', {
	    attrs: {
	      "newButton": true,
	      "heading": treeListHeading,
	      "items": trees
	    },
	    on: {
	      "new": function($event) {
	        newTree()
	      },
	      "itemselected": treeSelected
	    }
	  }) : _h('spinner'), " "])])
	}},staticRenderFns: []}

/***/ },
/* 280 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', {
	    staticClass: "d3-tree-visual"
	  }, [_h('div', {
	    staticClass: "svg-container",
	    class: svgColSize
	  }, [_h('circlepanelmanager'), " ", (!tree) ? _h('spinner') : _e(), " ", _h('svg', {
	    staticClass: "tree",
	    class: {
	      'patient': isPatientChosen, 'no-patient': !isPatientChosen
	    },
	    attrs: {
	      "width": svgWidth,
	      "height": "500"
	    }
	  }), " ", (infopanelCollapsed) ? _h('button', {
	    staticClass: "btn btn-default infopanel-toggler",
	    on: {
	      "click": function($event) {
	        toggleInfopanel()
	      }
	    }
	  }, [_m(0)]) : _e()]), " ", (!infopanelCollapsed) ? _h('div', {
	    staticClass: "col-md-3"
	  }, [_h('infopanel', {
	    on: {
	      "toggle": toggleInfopanel
	    }
	  })]) : _e()])
	}},staticRenderFns: [function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-chevron-left"
	  })
	}}]}

/***/ },
/* 281 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _m(0)
	}},staticRenderFns: [function (){with(this) {
	  return _h('div', {
	    staticClass: "spaced"
	  }, [_h('div', {
	    staticClass: "col-md-offset-3 col-md-6"
	  }, [_h('h2', ["404 - That page you requested was not found."])])])
	}}]}

/***/ },
/* 282 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', [_h('div', {
	    staticClass: "modal-backdrop fade",
	    class: { in: visible
	    }
	  }), " ", _h('div', {
	    staticClass: "modal fade",
	    class: { in: visible
	    },
	    attrs: {
	      "tabindex": "-1",
	      "role": "dialog"
	    }
	  }, [_h('div', {
	    staticClass: "modal-dialog",
	    attrs: {
	      "role": "document"
	    }
	  }, [_h('div', {
	    staticClass: "modal-content"
	  }, [_h('div', {
	    staticClass: "modal-header",
	    class: headerClass
	  }, [_h('button', {
	    staticClass: "close",
	    attrs: {
	      "type": "button",
	      "aria-label": "Close"
	    },
	    on: {
	      "click": cancel
	    }
	  }, [_m(0)]), " ", _h('h4', {
	    staticClass: "modal-title"
	  }, [_s(title)])]), " ", _h('div', {
	    staticClass: "modal-body"
	  }, [_t("default", ["Please confirm this action."])]), " ", _h('div', {
	    staticClass: "modal-footer"
	  }, [_h('button', {
	    staticClass: "btn btn-default",
	    attrs: {
	      "type": "button"
	    },
	    on: {
	      "click": cancel
	    }
	  }, ["Cancel"]), " ", _h('button', {
	    staticClass: "btn",
	    class: buttonClass,
	    attrs: {
	      "type": "button"
	    },
	    on: {
	      "click": confirmIt
	    }
	  }, ["Confirm"])])])])])])
	}},staticRenderFns: [function (){with(this) {
	  return _h('span', {
	    attrs: {
	      "aria-hidden": "true"
	    }
	  }, ["×"])
	}}]}

/***/ },
/* 283 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', [_h('transition', {
	    attrs: {
	      "appear": "",
	      "name": "circle-menu"
	    }
	  }, [(node) ? _h('div', {
	    staticClass: "circle-panel-container"
	  }, [_h('nav', {
	    staticClass: "c-circle-menu js-menu",
	    style: ({
	      top: top,
	      left: left
	    }),
	    on: {
	      "click": function($event) {
	        if ($event.target !== $event.currentTarget) return;
	        deselect()
	      }
	    }
	  }, [_h('ul', {
	    staticClass: "c-circle-menu__items"
	  }, [_h('li', {
	    staticClass: "c-circle-menu__item"
	  }, [_h('a', {
	    staticClass: "c-circle-menu__link",
	    attrs: {
	      "title": "Toggle display of Children"
	    },
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        toggle()
	      }
	    }
	  }, [_m(0)])]), " ", _h('li', {
	    staticClass: "c-circle-menu__item"
	  }, [_h('a', {
	    staticClass: "c-circle-menu__link",
	    attrs: {
	      "title": "Insert a new Node after this Node"
	    },
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        newNode()
	      }
	    }
	  }, [_m(1)])]), " ", _h('li', {
	    staticClass: "c-circle-menu__item"
	  }, [_h('a', {
	    staticClass: "c-circle-menu__link",
	    attrs: {
	      "title": "Move this node to a new Parent"
	    },
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        moveNode()
	      }
	    }
	  }, [_m(2)])]), " ", _h('li', {
	    staticClass: "c-circle-menu__item"
	  }, [_h('a', {
	    staticClass: "c-circle-menu__link",
	    attrs: {
	      "title": "Delete this Node"
	    },
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        clickDelete()
	      }
	    }
	  }, [_m(3)])]), " ", _h('li', {
	    staticClass: "c-circle-menu__item"
	  }, [_h('a', {
	    staticClass: "c-circle-menu__link",
	    attrs: {
	      "title": "Close Menu"
	    },
	    on: {
	      "click": function($event) {
	        $event.stopPropagation();
	        deselect()
	      }
	    }
	  }, [_m(4)])])])])]) : _e()]), " ", (confirmationTitle) ? _h('confirmation', {
	    attrs: {
	      "type": "alert",
	      "title": confirmationTitle
	    },
	    on: {
	      "cancel": function($event) {
	        cancelAction()
	      },
	      "confirm": function($event) {
	        deleteNode()
	      }
	    }
	  }, [_h('p', ["Are you sure you want to delete \"" + _s(node.data.name) + "\" forever?"]), " ", _h('p', {
	    directives: [{
	      name: "show",
	      rawName: "v-show",
	      value: (node.data.children.length),
	      expression: "node.data.children.length"
	    }],
	    staticClass: "alert alert-danger"
	  }, ["\n      This will also delete all children of the node!\n    "])]) : _e()])
	}},staticRenderFns: [function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-caret-right"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-plus"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-scissors"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-trash"
	  })
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-times"
	  })
	}}]}

/***/ },
/* 284 */
/***/ function(module, exports) {

	module.exports={render:function (){with(this) {
	  return _h('div', [_m(0), " ", (!creating) ? _h('div', {
	    staticClass: "row"
	  }, [_h('div', {
	    staticClass: "col-md-6"
	  }, [_m(1), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_m(2), " ", _h('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (newTreeData.name),
	      expression: "newTreeData.name"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "text"
	    },
	    domProps: {
	      "value": _s(newTreeData.name)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        newTreeData.name = $event.target.value
	      }
	    }
	  })]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_h('a', {
	    staticClass: "btn btn-success form-control",
	    on: {
	      "click": newTree
	    }
	  }, [_m(3), " New Tree "])])]), " ", _h('div', {
	    staticClass: "col-md-6"
	  }, [_m(4), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_m(5), " ", _h('input', {
	    directives: [{
	      name: "model",
	      rawName: "v-model",
	      value: (copyTreeData.name),
	      expression: "copyTreeData.name"
	    }],
	    staticClass: "form-control",
	    attrs: {
	      "type": "text"
	    },
	    domProps: {
	      "value": _s(copyTreeData.name)
	    },
	    on: {
	      "input": function($event) {
	        if ($event.target.composing) return;
	        copyTreeData.name = $event.target.value
	      }
	    }
	  })]), " ", _h('div', {
	    staticClass: "form-group"
	  }, [_h('a', {
	    staticClass: "btn btn-success form-control",
	    on: {
	      "click": copyTree
	    }
	  }, [_m(6), " Copy Tree "])]), " ", (trees) ? _h('selection-list', {
	    attrs: {
	      "heading": treeListHeading,
	      "items": trees,
	      "activeItem": selectedTree
	    },
	    on: {
	      "itemselected": treeSelected
	    }
	  }) : _e()])]) : _e(), " ", (creating) ? _h('spinner') : _e()])
	}},staticRenderFns: [function (){with(this) {
	  return _h('div', {
	    staticClass: "row"
	  }, [_h('h2', ["Create a new Tree"]), " ", _h('p', ["\n      You can either create an entirely new tree and then add nodes to it.\n      Or you can create a copy of an existing tree to adapt it to your needs.\n    "])])
	}},function (){with(this) {
	  return _h('h2', ["Create a new empty Tree"])
	}},function (){with(this) {
	  return _h('label', ["Name:"])
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-plus"
	  })
	}},function (){with(this) {
	  return _h('h2', ["Create a Copy of an existing Tree"])
	}},function (){with(this) {
	  return _h('label', ["Name of Copy:"])
	}},function (){with(this) {
	  return _h('i', {
	    staticClass: "fa fa-lg fa-copy"
	  })
	}}]}

/***/ }
]);