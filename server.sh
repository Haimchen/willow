#!/bin/bash
PROJECT_ROOT=`dirname $0`

echo "Starting server on http://localhost:9999. Ctrl-C to quit."

ruby -run -ehttpd ${PROJECT_ROOT}/dist/ -p9999
